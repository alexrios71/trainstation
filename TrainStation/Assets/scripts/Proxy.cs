﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proxy : MonoBehaviour {

    public float increment;
    public float maxRad;
    public GameObject detectorProxy;

    private bool active;
    private float minRadius;
    private SphereCollider sc;

	// Use this for initialization
	void Start () {
        active = false;
        sc = GetComponent<SphereCollider>();
        minRadius = sc.radius;
	}
	
	// Update is called once per frame
	void Update () {
		if (active)
        {
            if (sc.radius < maxRad) sc.radius += increment * Time.deltaTime;
        }
        else
        {
            if (sc.radius > minRadius) sc.radius -= increment * Time.deltaTime;
            else 
            {
                //sc.radius = minRadius;
                gameObject.SetActive(false);
            }
        }
	}

    public void setAct(bool b)
    {
        active = b;
    }

    public bool isActive()
    {
        return active;
    }

    public float getMinRadius()
    {
        return minRadius;
    }

    public bool isInsideList(GameObject c)
    {
        return detectorProxy.GetComponent<detectorProxy>().isInList(c);
    }
}
