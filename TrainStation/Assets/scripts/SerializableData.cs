﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SerializableData {

    [SerializeField]
    public List<Vector3> pos;
    [SerializeField]
    public List<float> time;
    [SerializeField]
    public Vector3 posPlayer;
    [SerializeField]
    public float rotYPlayer;
    [SerializeField]
    public float timePlayer;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
