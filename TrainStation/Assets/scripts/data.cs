﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class data : MonoBehaviour {

    public int chars;
    public float genTime;
    public InputField c;
    public InputField gt;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void startScene()
    {
        chars = int.Parse(c.text);
        genTime = float.Parse(gt.text);
        SceneManager.LoadScene("scene5");
    }
}
