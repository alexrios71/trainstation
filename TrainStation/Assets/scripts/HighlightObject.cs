﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightObject : MonoBehaviour {

    Material[] mats;
    bool highlight;

	// Use this for initialization
	void Start () {
        highlight = false;
        mats = GetComponent<Renderer>().materials;
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("holaaaaaaaa");
		if (highlight)
        {
            foreach (Material c in mats)
            {
                c.shader = Shader.Find("Self-Illumin/Outlined Diffuse");
            }
        }
        else
        {
            foreach (Material c in mats)
            {
                c.shader = Shader.Find("Standard");
            }
        }
	}

    public void setHighlight(bool b)
    {
        highlight = b;
       
    }
}
