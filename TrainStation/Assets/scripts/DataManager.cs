﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour {

    public Sprite play;
    public Sprite pause;
    public GameObject player;
    public GameObject NPC;
    public float speed;
    public InputField execution;
    public GameObject executionIdText;
    public GameObject dateExecutionText;

    private GameObject velText;
    private GameObject velSlider;
    private GameObject start;
    private GameObject timeline;
    private GameObject iniPath;
    private GameObject endPath;
    private GameObject timer;
    private LineRenderer lineMaker;
    private bool running;
    private float framerate;
    private float totalTime;
    private float actTime;
    private List<GameObject> activeNPCs;
    private GameObject activePlayer;
    [SerializeField]
    private List<SerializableData> frames;
    private BinaryFormatter bf;
    private int execId = 0;
    private string dateAndTime;

	// Use this for initialization
	void Start () {
        lineMaker = GetComponent<LineRenderer>();
        velText = GameObject.Find("ActualSpeed");
        velSlider = GameObject.Find("SpeedSet");
        start = GameObject.Find("Play");
        timeline = GameObject.Find("TimeLine");
        iniPath = GameObject.Find("InputStartSec");
        endPath = GameObject.Find("InputEndSec");
        timer = GameObject.Find("Timer");
        actTime = 0.0f;
        running = false;
        initSerializableData();
        loadData();
        activeNPCs = new List<GameObject>();
        activePlayer = Instantiate(player);
        updateTimer();
        drawPoints();
        executionIdText.GetComponent<Text>().text = "Execution ID: 0";
        dateExecutionText.GetComponent<Text>().text = dateAndTime;
    }
	
	// Update is called once per frame
	void Update () {
		if (running)
        {
            if (actTime > framerate)
            {
                if (timeline.GetComponent<Slider>().value >= timeline.GetComponent<Slider>().maxValue)
                {
                    start.GetComponent<Image>().sprite = play;
                    running = false;
                }
                else
                {
                    ++timeline.GetComponent<Slider>().value;
                    drawPoints();
                    updateTimer();
                    actTime = 0.0f;
                }
            }
            else actTime += Time.deltaTime*speed;
        }
	}

    public void clickButton()
    {
        if (running)
        {
            start.GetComponent<Image>().sprite = play;
            running = false;
        }
        else
        {
            start.GetComponent<Image>().sprite = pause;
            running = true;
        }
    }

    private void loadData()
    {
        if (File.Exists(Application.persistentDataPath + "/npcData" + execId + ".dat"))
        {
            FileStream file = File.Open(Application.persistentDataPath + "/npcData" + execId + ".dat", FileMode.Open);

            frames = (List<SerializableData>)bf.Deserialize(file);
            file.Close();
        }

        if (File.Exists(Application.persistentDataPath + "/variablesData" + execId + ".dat"))
        {
            StreamReader file = new StreamReader(Application.persistentDataPath + "/variablesData" + execId + ".dat");
            string fr;
            if ((fr = file.ReadLine()) != null) framerate = float.Parse(fr);
            if ((fr = file.ReadLine()) != null) totalTime = float.Parse(fr);
            if ((fr = file.ReadLine()) != null) execId = int.Parse(fr);
            if ((fr = file.ReadLine()) != null) dateAndTime = fr;
            file.Close();
        }

        timeline.GetComponent<Slider>().maxValue = frames.Count - 1;
    }

    private void initSerializableData()
    {
        bf = new BinaryFormatter();
        SurrogateSelector surrogateSelector = new SurrogateSelector();
        Vector3SerializationSurrogate vector3SS = new Vector3SerializationSurrogate();

        surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3SS);
        bf.SurrogateSelector = surrogateSelector;
    }

    private void drawPoints()
    {
        int i = (int) timeline.GetComponent<Slider>().value;
        int dif = activeNPCs.Count - frames[i].pos.Count;
        if (dif < 0)
        {
            //N'hi ha de menys, s'han d'afegir esferes
            for (int j = 0; j < -dif; ++j)
            {
                GameObject s = Instantiate(NPC);
                activeNPCs.Add(s);
            }
        }
        else if (dif > 0)
        {
            //N'hi ha de mes, s'han de restar esferes
            for (int j = 0; j < dif; ++j) Destroy(activeNPCs[j]);
            activeNPCs.RemoveRange(0,dif);
        }

        for (int j = 0; j < activeNPCs.Count; ++j)
        {
            activeNPCs[j].transform.position = new Vector3(frames[i].pos[j].x, 1.0f, frames[i].pos[j].z);
        }

        if (activePlayer == null) activePlayer = Instantiate(player);
        activePlayer.transform.position = new Vector3(frames[i].posPlayer.x, 1.0f, frames[i].posPlayer.z);
        activePlayer.transform.localEulerAngles = new Vector3(activePlayer.transform.localEulerAngles.x, frames[i].rotYPlayer, activePlayer.transform.localEulerAngles.z);
    }

    private void updateTimer()
    {
        timer.GetComponent<Text>().text = (timeline.GetComponent<Slider>().value * framerate) + " / " + (timeline.GetComponent<Slider>().maxValue * framerate);
    }

    public void beginDrag()
    {
        start.GetComponent<Image>().sprite = play;
        running = false;
    }

    public void endDrag()
    {
        updateTimer();
        drawPoints();
    }

    public void setVelocity()
    {
        velText.GetComponent<Text>().text = velSlider.GetComponent<Slider>().value.ToString();
        speed = velSlider.GetComponent<Slider>().value;
    }

    public void drawPath()
    {
        float iniSec = float.Parse(iniPath.GetComponent<InputField>().text);
        float endSec = float.Parse(endPath.GetComponent<InputField>().text);

        if (iniSec >= endSec) return;
        if (iniSec < 0) { iniSec = 0; iniPath.GetComponent<InputField>().text = "0"; }
        else if (iniSec > totalTime) { iniSec = totalTime; iniPath.GetComponent<InputField>().text = totalTime.ToString(); }
        if (endSec < 0) { endSec = 0; endPath.GetComponent<InputField>().text = "0"; }
        else if (endSec > totalTime) { endSec = totalTime; endPath.GetComponent<InputField>().text = totalTime.ToString(); }

        iniSec = ((int)(iniSec * 10)) / 10.0f;
        endSec = ((int)(endSec * 10)) / 10.0f;
        int posIni = (int)(iniSec / framerate);
        int posEnd = (int)(endSec / framerate);
        int i = posIni;

        Vector3[] positions = new Vector3[posEnd-posIni+1];
        for (; posIni <= posEnd; ++posIni)
        {
            positions[posIni - i] = new Vector3(frames[posIni].posPlayer.x, 1.0f, frames[posIni].posPlayer.z);
        }
        lineMaker.positionCount = positions.Length;
        lineMaker.SetPositions(positions);

        if (running)
        {
            running = false;
            start.GetComponent<Image>().sprite = play;
        }
    }

    public void cleanPath()
    {
        lineMaker.positionCount = 0;
    }

    public void cleanPoints()
    {
        Destroy(activePlayer);
        for (int i = 0; i < activeNPCs.Count; ++i) Destroy(activeNPCs[i]);
        activeNPCs.Clear();
    }

    public void loadExecution()
    {
        cleanPath();
        cleanPoints();
        execId = int.Parse(execution.text);
        actTime = 0.0f;
        running = false;
        initSerializableData();
        loadData();
        activeNPCs = new List<GameObject>();
        activePlayer = Instantiate(player);
        updateTimer();
        drawPoints();
        executionIdText.GetComponent<Text>().text = "Execution ID: " + execId;
        dateExecutionText.GetComponent<Text>().text = dateAndTime;
    }
}
