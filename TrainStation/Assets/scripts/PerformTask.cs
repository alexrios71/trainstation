﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerformTask : MonoBehaviour
{
    public Vector3 Height;
   // private Animator anim;
 
    private bool isMakingAction;
    private GameObject pointer;
    private string[] interactables = { "toilet", "book", "shirts", "fruit", "tool", "furniture", "ball", "kitchen",
        "printer", "speakers", "tv", "pc", "drink", "cashmachine", "vendingticket" };
    private int taskNumber;
    private bool[] hasItem;
    private GameObject lastObj;

    // Use this for initialization
    void Start()
    {
        
        taskNumber = 0;
        //anim = player.GetComponent<Animator>();
        hasItem = new bool[interactables.Length];
        for (int i = 0; i < hasItem.Length; ++i) hasItem[i] = false;

        //Pointer for right hand
        pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //pointer.transform.parent = rightHand.transform;
        pointer.transform.localScale = new Vector3(0.5f, 0.001f, 0.001f);
        pointer.transform.localPosition = new Vector3(-0.15f, 0f, 0f);
        pointer.transform.localRotation = Quaternion.identity;
        BoxCollider collider = pointer.GetComponent<BoxCollider>();
        Destroy(collider);

        Material newMaterial = new Material(Shader.Find("Unlit/Color"));
        Color color = new Color(0f, 0f, 0f);
        newMaterial.SetColor("_Color", color);
        pointer.GetComponent<MeshRenderer>().material = newMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log(this.gameObject.name);
        /*if (isMakingAction)
        {
            if (anim.GetInteger("Action") == 0)
            {
                isMakingAction = false;
                anim.SetInteger("State", 0);
                anim.speed = 1.0f;
                //player.GetComponent<MovementPlayer>().enableRotation();
            }
        }*/
        // device1 = SteamVR_Controller.Input((int)rightCtr.index);
        //UnityEngine.Debug.DrawLine(transform.position, (transform.position+(transform.forward * 2.5f)) );
        if (Input.GetButton("TriggersR_0") || Input.GetButton("TriggersR_1"))
        {
            //print("button"+ (transform.position+ transform.forward));
           
            RaycastHit hit;
            if (Physics.Raycast(transform.position + Height, transform.forward, out hit, 2.5f))
            {
                //Vector3 pos = transform.position + new Vector3()*transform.forward;
                Debug.DrawLine(transform.position + Height, hit.point);
                //Debug.Log(hit.point);
                    string thing = hit.transform.tag;
                    if (thing == "door")
                    {
                        if (hit.transform.GetComponent<Animator>().GetInteger("State") == 0)
                        {
                            hit.transform.GetComponent<Animator>().SetInteger("State", 1);
                        }
                        else
                        {
                            hit.transform.GetComponent<Animator>().SetInteger("State", 0);
                        }
                    }
                    else if (taskNumber < 5)
                    {
                        if ((thing == "cashmachine" || thing == "vendingticket" || thing == "drink") && itemInInventory(thing) && hit.distance < 0.7f)
                        {
                            GameManager.playItemSound(0);
                            /*isMakingAction = true;
                            anim.SetInteger("State", 2);
                            anim.SetInteger("Action", 6);
                            anim.speed = 2.0f;
                            player.GetComponent<MovementPlayer>().disableRotation();*/
                            saveActionData(thing);
                        }
                        else if (thing == "toilet" && !hasItem[0] && hit.distance < 0.7f)
                        {
                            hasItem[0] = true;
                            GameManager.activateAudio(hit.transform, "toilet");
                            saveActionData(thing);
                        }
                        else if (thing != "Untagged" && itemInInventory(thing) && hit.distance < 1.0f)
                        {
                            GameManager.playItemSound(0);
                            /*isMakingAction = true;
                            anim.SetInteger("State", 2);
                            anim.SetInteger("Action", 6);
                            anim.speed = 2.0f;
                            player.GetComponent<MovementPlayer>().disableRotation();*/
                            saveActionData(thing);
                        }
                    
                }
                if (hit.transform.gameObject != lastObj && hit.transform.gameObject.layer == 12)
                {
                    lastObj = hit.transform.gameObject;
                    Renderer lastObjRend = lastObj.GetComponent<Renderer>();
                    if(lastObjRend != null)
                    {
                        //Material[] mats = hit.transform.gameObject.GetComponent<Renderer>().materials;
                        Material[] mats = lastObjRend.materials;
                        foreach (Material c in mats)
                        {
                            c.shader = Shader.Find("Self-Illumin/Outlined Diffuse");
                        }
                        // Debug.Log("Holaaa");
                    } else
                    {
                        // Object has no renderer. It is probabliy a vendor.
                        // @todo: play some animation?
                        // by: amyrneto@gmail.com
                    }
                    TasksManager.NextTask(lastObj.name);
                }
                else if (lastObj != null)
                {
                    lastObj = null;
                    Renderer lastObjRend = hit.transform.gameObject.GetComponent<Renderer>();
                    if(lastObjRend != null)
                    {
                        Material[] mats = hit.transform.gameObject.GetComponent<Renderer>().materials;
                        foreach (Material c in mats)
                        {
                            c.shader = Shader.Find("Standard");
                        }
                    }
                }
            }
            else if (lastObj != null)
            {
                Renderer lastObjRend = lastObj.GetComponent<Renderer>();
                if (lastObjRend != null)
                {
                    Material[] mats = lastObjRend.materials;
                    foreach (Material c in mats)
                    {
                        c.shader = Shader.Find("Standard");
                    }
                }
                lastObj = null;
            }
        }
    }

    private bool itemInInventory(string item)
    {
        for (int i = 0; i < interactables.Length; ++i)
        {
            if (item == interactables[i])
            {
                if (!hasItem[i])
                {
                    hasItem[i] = true;
                    return true;
                }
                else return false;
            }
        }
        return false;
    }

    private void saveActionData(string thing)
    {
        GameManager.AddAction(thing);
        if (taskNumber == 0) GameManager.StartCountdown();
        else if (taskNumber == 2) GameManager.StartEvacuating();
        ++taskNumber;
    }
}
