﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkCollision : MonoBehaviour {

    GameObject parent;

	// Use this for initialization
	void Awake () {
        parent = transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != null && !parent.GetComponent<ControllerMovementAI>().existsCollider(other.gameObject))
        {
            parent.GetComponent<ControllerMovementAI>().addNewCollider(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject != null && parent.GetComponent<ControllerMovementAI>().existsCollider(other.gameObject))
        {
            parent.GetComponent<ControllerMovementAI>().deleteCollider(other.gameObject);
        }
    }
}
