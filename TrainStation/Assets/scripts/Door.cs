﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {
    
    public GameObject[] doors;
    private Animator[] anims;
    private int objectsInCollision;

	// Use this for initialization
	void Start () {
        anims = new Animator[2];
        anims[0] = doors[0].GetComponent<Animator>();
        anims[1] = doors[1].GetComponent<Animator>();
        objectsInCollision = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (objectsInCollision == 0)
        {
            anims[0].SetInteger("State", 2);
            anims[1].SetInteger("State", 2);
        }
        else
        {
            anims[0].SetInteger("State", 1);
            anims[1].SetInteger("State", 1);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "character" || other.tag == "Player") ++objectsInCollision;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "character" || other.tag == "Player") --objectsInCollision;
    }
}
