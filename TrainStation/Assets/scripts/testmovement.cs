﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class testmovement : MonoBehaviour {

    public Transform obj;
    public AudioClip[] m_FootstepSounds;
    private AudioSource m_AudioSource;

    private Animator anim;
    private NavMeshAgent agent;
    private NavMeshObstacle obstacle;
    //private float t;
    private float leftFootPrev;
    private float leftFootAct;
    private float rightFootPrev;
    private float rightFootAct;

    const float MaxRotSpeed = 0.3f;

	// Use this for initialization
	void Start () {
        //t = 0.0f;
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        m_AudioSource = GetComponent<AudioSource>();
        //obstacle = GetComponent<NavMeshObstacle>();
    }
	
	// Update is called once per frame
	void Update () {

        //Debug.Log("char: " + transform.eulerAngles);
        //Debug.Log(agent.hasPath);
        if (Input.GetKeyDown("r")) Debug.Log(obj.eulerAngles);
        if (Input.GetKeyDown("f")) Debug.Log(transform.eulerAngles);
        

        //anim.SetFloat("Velocity", Mathf.Lerp(0.0f, MaxRotSpeed, t));
        RaycastHit hit;

        /*if (Input.GetMouseButtonDown(2))
        {
            agent.enabled = false;
            obstacle.enabled = true;
        }*/

        //Debug.Log(agent.hasPath);

        if (Input.GetMouseButtonDown(0)) {
            //obstacle.enabled = false;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                agent.destination = hit.point;
            }
        }

        if (Input.GetMouseButtonDown(2))
        {
            agent.areaMask = -1;
        }

        //anim.SetFloat("Direction", 0.3f);
        /*if (!agent.hasPath && anim.GetFloat("Forward") < 0.1f)
        {
            agent.enabled = false;
            obstacle.enabled = true;
        }
        else
        {
            obstacle.enabled = false;
            agent.enabled = true;
        }*/
        //CheckFootstep();
    }

    private void CheckFootstep()
    {
        leftFootAct = anim.GetFloat("LeftFoot");
        rightFootAct = anim.GetFloat("RightFoot");
        //check left foot frame
        if (leftFootAct >= 0 && leftFootPrev < 0) footstep();
        if (rightFootAct <= 0 && rightFootPrev > 0) footstep();
        leftFootPrev = anim.GetFloat("LeftFoot");
        rightFootPrev = anim.GetFloat("RightFoot");
    }

    private void footstep()
    {
        int n = Random.Range(0, m_FootstepSounds.Length);
        m_AudioSource.clip = m_FootstepSounds[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
    }

    public void resetAction()
    {
        anim.SetInteger("Action", 0);
    }
}
