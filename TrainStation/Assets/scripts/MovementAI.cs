﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]
public class MovementAI : MonoBehaviour
{
    [SerializeField]
    float m_MovingTurnSpeed = 360;
    [SerializeField]
    float m_StationaryTurnSpeed = 180;
    [SerializeField]
    float m_MoveSpeedMultiplier = 1f;
    [SerializeField]
    float m_AnimSpeedMultiplier = 1f;
    [SerializeField]
    float m_GroundCheckDistance = 0.1f;

    const float MAX_AVOIDANCE_ANGLE = 90.0f;

    Rigidbody m_Rigidbody;
    Animator m_Animator;
    bool m_IsGrounded;
    const float k_Half = 0.5f;
    float m_TurnAmount;
    float m_ForwardAmount;
    Vector3 m_GroundNormal;
    CapsuleCollider m_Capsule;
    bool m_Crouching;
    private bool isInside;
    private bool avoid;
    private float timerProxy;
    private bool hasExitProxy;
    private List<GameObject> collidersNear;
    private Vector3 direction;

    void Awake()
    {
        timerProxy = 2.0f;
        isInside = true;
        avoid = false;
        hasExitProxy = false;
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        collidersNear = new List<GameObject>();
        direction = new Vector3(0f, 0f, 0f);
    }


    public void Move(Vector3 move, bool crouch, bool jump)
    {
        // convert the world relative moveInput vector into a local relative
        // turn amount and forward amount required to head in the desired
        // direction.
        float firstMagnitude = move.magnitude;
        if (move.magnitude > 1f)
        {
            move.Normalize();
            firstMagnitude = move.magnitude;
        }
        move = DetectCollision(move, firstMagnitude);
        direction = move;
        move = transform.InverseTransformDirection(move);
        CheckGroundStatus();
        move = Vector3.ProjectOnPlane(move, m_GroundNormal);
        
        m_TurnAmount = Mathf.Atan2(move.x, move.z);
        m_ForwardAmount = move.z;
        if (avoid) m_ForwardAmount = -move.z;
        else if (!hasExitProxy) m_ForwardAmount = move.z;
        else
        {
            m_ForwardAmount = 0.0f;
            timerProxy -= Time.deltaTime;
            if (timerProxy <= 0)
            {
                timerProxy = 2.0f;
                hasExitProxy = false;
            }
        }

        ApplyExtraTurnRotation();

        // send input and other state parameters to the animator
        UpdateAnimator(move);
    }

    void UpdateAnimator(Vector3 move)
    {
        // update the animator parameters
        m_Animator.SetFloat("Velocity", m_ForwardAmount, 0.1f, Time.deltaTime);
        m_Animator.SetFloat("Direction", m_TurnAmount, 0.1f, Time.deltaTime);
        
        if (m_IsGrounded && move.magnitude > 0)
        {
            m_Animator.speed = m_AnimSpeedMultiplier;
        }
    }

    void ApplyExtraTurnRotation()
    {
        // help the character turn faster (this is in addition to root rotation in the animation)
        float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
        transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
    }


    public void OnAnimatorMove()
    {
        // we implement this function to override the default root motion.
        // this allows us to modify the positional speed before it's applied.
        if (m_IsGrounded && Time.deltaTime > 0)
        {
            Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

            // we preserve the existing y part of the current velocity.
            v.y = m_Rigidbody.velocity.y;
            m_Rigidbody.velocity = v;
        }
    }


    void CheckGroundStatus()
    {
        RaycastHit hitInfo;
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
#endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
        {
            m_GroundNormal = hitInfo.normal;
            m_IsGrounded = true;
            //m_Animator.applyRootMotion = true;
        }
    }

    public void setInside(bool b)
    {
        isInside = b;
    }

    public bool getInside()
    {
        return isInside;
    }

    void OnTriggerEnter(Collider other)
    {
        /*if (other.tag == "proxy" && !isInside)
        {
            avoid = true;
        }*/
    }

    void OnTriggerExit(Collider other)
    {
        /*if (other.tag == "proxy" && !isInside && !other.GetComponent<Proxy>().isInsideList(gameObject))
        {
            hasExitProxy = true;
            avoid = false;
        }*/
    }

    private Vector3 DetectCollision(Vector3 move, float magnitude)
    {
        GameObject col = GetNearestCollision();
        Vector3 moveNorm = move;
        if (col != null)
        {
            if (col.transform.parent.tag == "character")
            {
                Vector3 vecColliders = (col.transform.position - transform.position).normalized;
                float angle = Vector3.Angle(vecColliders, moveNorm);
                if (angle < MAX_AVOIDANCE_ANGLE) //needs to avoid if he's going to the obstacle
                {
                    //Get closest angle where the agent has to go (from the two perpendicular vectors
                    //we got from the center of the obstacle and the center of the agent)
                    Vector3 perp1 = new Vector3(-vecColliders.z, 0f, vecColliders.x);   //left perpendicular
                    Vector3 perp2 = new Vector3(vecColliders.z, 0f, -vecColliders.x);   //right perpendicular

                    //Get closest vector to de direction vector
                    if (Vector3.Angle(perp1, moveNorm) < Vector3.Angle(perp2, moveNorm))
                    {
                        string obstacleDir = col.GetComponentInParent<MovementAI>().getDirection(transform.position);
                        //string obstacleDir = "left";
                        if (obstacleDir == "left")
                        {
                            moveNorm = moveNorm.normalized + perp1 * Random.Range(1f, 1.2f);
                            moveNorm = moveNorm.normalized * magnitude;
                        }
                        else
                        {
                            moveNorm = new Vector3(0f, 0f, 0f);
                        }

                    }
                    else
                    {
                        moveNorm = moveNorm.normalized + perp2 * Random.Range(1f, 1.2f);
                        moveNorm = moveNorm.normalized * magnitude;
                    }
                }
            }
            else if (col.transform.parent.tag == "Player")
            {
                Vector3 vecColliders = (col.transform.position - transform.position).normalized;
                float angle = Vector3.Angle(vecColliders, moveNorm);
                if (angle < MAX_AVOIDANCE_ANGLE) //needs to avoid if he's going to the obstacle
                {
                    //Get closest angle where the agent has to go (from the two perpendicular vectors
                    //we got from the center of the obstacle and the center of the agent)
                    Vector3 perp1 = new Vector3(-vecColliders.z, 0f, vecColliders.x);   //left perpendicular
                    Vector3 perp2 = new Vector3(vecColliders.z, 0f, -vecColliders.x);   //right perpendicular

                    //Get closest vector to de direction vector
                    if (Vector3.Angle(perp1, moveNorm) < Vector3.Angle(perp2, moveNorm))
                    {
                        moveNorm = moveNorm.normalized + perp1 * Random.Range(1f, 1.2f);
                        moveNorm = moveNorm.normalized * magnitude;
                    }
                    else
                    {
                        moveNorm = moveNorm.normalized + perp2 * Random.Range(1f, 1.2f);
                        moveNorm = moveNorm.normalized * magnitude;
                    }
                }
            }
        }
        return moveNorm;
    }

    public bool existsCollider(GameObject col)
    {
        return (collidersNear.Contains(col));
    }

    public void addNewCollider(GameObject col)
    {
        collidersNear.Add(col);
    }

    public void deleteCollider(GameObject col)
    {
        collidersNear.Remove(col);
    }

    private GameObject GetNearestCollision()
    {
        if (collidersNear.Count == 0) return null;
        GameObject res = collidersNear[0];
        int i = 1;
        while (i < collidersNear.Count)
        {
            if (res == null)
            {
                res = collidersNear[i];
                ++i;
            }
            else
            {
                if (collidersNear[i] == null)
                {
                    collidersNear.RemoveAt(i);
                }
                else
                {
                    if (Vector3.Distance(collidersNear[i].transform.position, transform.position) < Vector3.Distance(res.transform.position, transform.position))
                    {
                        res = collidersNear[i];
                    }
                    ++i;
                }
            }
        }
        return res;
    }

    public string getDirection(Vector3 pos)
    {
        Vector3 vecColliders = (pos - transform.position).normalized;
        Vector3 perp1 = new Vector3(-vecColliders.z, 0f, vecColliders.x);   //left perpendicular
        Vector3 perp2 = new Vector3(vecColliders.z, 0f, -vecColliders.x);   //right perpendicular
        if (Vector3.Angle(perp1, direction) <= Vector3.Angle(perp2, direction)) return "left";
        else return "right";
    }
}
