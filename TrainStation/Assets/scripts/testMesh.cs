﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testMesh : MonoBehaviour {

    public Color Color1, Color2;
    public float Speed = 1, Offset;

    private Renderer _renderer;
    private MaterialPropertyBlock _propBlock;

    void Awake()
    {
        _propBlock = new MaterialPropertyBlock();
        _renderer = GetComponent<Renderer>();
    }

    void Update()
    {
        // Get the current value of the material properties in the renderer.
        
        // Assign our new value.
        if (Input.GetKey(KeyCode.O))
        {
            _renderer.GetPropertyBlock(_propBlock);
            _propBlock.SetColor("_Color", Color1);
            _renderer.SetPropertyBlock(_propBlock);
        }
        else
        {
            _renderer.GetPropertyBlock(_propBlock);
            _propBlock.SetColor("_Color", Color2);
            _renderer.SetPropertyBlock(_propBlock);
        }
        // Apply the edited values to the renderer.
        
    }
}
