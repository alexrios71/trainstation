﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TasksManager : MonoBehaviour {

    private ArrayList tasksList;
    private int nexttask;

    public String taskname;

    public GameObject panel;
    public GameObject tasktext;
  
    public float panelTimeOut;

    public static TasksManager instance = null;


    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }    // Use this for initialization
    void Start () {
        nexttask = 0;
        instance.tasksList = new ArrayList();
        instance.tasksList.Add("Look for a ticket machine");
        instance.tasksList.Add("Look for a drinks machine");
        instance.tasksList.Add("Look for a green drinks machine");
        instance.tasksList.Add("Go to platform 1");
        instance.tasksList.Add("Find an exit");
        instance.taskname = null;
        NextTask("ticket");
        
	}

    public void Update()
    {
        Debug.Log("taskname:" + taskname);
        if (Input.GetKeyDown(KeyCode.L))
        {
            NextTask("Tasca" + nexttask) ;
        }

    }

    public static void NextTask(String s)
    {
        //Debug.Log(instance.taskname + "  " + instance.nexttask);
        if (s != instance.taskname)
        {
            instance.taskname = s;
            if (instance.nexttask < instance.tasksList.Count)
            {
                instance.panel.SetActive(true);
                instance.tasktext.GetComponent<Text>().text = instance.tasksList[instance.nexttask].ToString();
                instance.Invoke("HidePanel", instance.panelTimeOut);
                instance.nexttask++;
            }
            if (instance.nexttask == instance.tasksList.Count)
                GameManager.StartEvacuating();
        }
    }


    public void HidePanel()
    {
        panel.SetActive(false);
    }
}
