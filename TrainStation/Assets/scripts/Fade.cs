﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour {

    public Texture2D fadeTex;
    public float speed = 0.5f;

    private int order = -1000;
    private float alpha = 1.0f;
    private int fade = -1;

    void OnGUI()
    {
        alpha += fade * speed * Time.deltaTime;
        alpha = Mathf.Clamp01(alpha);

        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = order;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTex);
    }

    public float startFade(int f)
    {
        fade = f;
        return (speed);
    }

    public void OnLevelWasLoaded()
    {
        startFade(-1);
    }
}
