﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BoxOut : MonoBehaviour {

    private List<GameObject> charsToLeave;

	// Use this for initialization
	void Start () {
        charsToLeave = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
		
        //If there is people that went outside but it's still in the leaving zone, check everyone until
        //they are outside and change inside state.
        if (charsToLeave.Count > 0)
        {
            for (int i = 0; i < charsToLeave.Count; ++i)
            {
                NavMeshAgent agent = charsToLeave[i].GetComponent<NavMeshAgent>();
                NavMeshHit hit;
                if (!NavMesh.SamplePosition(agent.transform.position, out hit, 0.1f, 32))
                {
                    charsToLeave[i].GetComponent<ControllerMovementAI>().setInside(false);
                    agent.areaMask = (21 | (agent.areaMask & 8));
                    charsToLeave.RemoveAt(i);
                }
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "character" && other.gameObject.GetComponent<ControllerMovementAI>().getInside())
        {
            NavMeshAgent agent = other.gameObject.GetComponent<NavMeshAgent>();
            NavMeshHit hit;
            if (!NavMesh.SamplePosition(agent.transform.position, out hit, 0.1f, 32))
            {
                other.gameObject.GetComponent<ControllerMovementAI>().setInside(false);
                agent.areaMask = (21 | (agent.areaMask & 8));
            }
            else
            {
                charsToLeave.Add(other.gameObject);
            }
        }
    }
}
