﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class CharacterControllerPHD : MonoBehaviour {
    
    private const float TICKETTIME = 6.0f;
    private const float BUYTIME = 6.0f;
    private const float STOPPINGDIST = 0.2f;
    private const float TIMETOACTION = 6.0f;
    private const float ISONPHONEPERCENTAGE = 0.1f;
    private const float SAVETIME = 0.5f;

    private Animator anim;
    private NavMeshAgent agent;
	private Vector3 destination;
	private bool gotDestination;
	private float timetoidle;
    private float timetoaction;
	private float timetogetticket;
    private float timesitting;
    private float timetobuy;
    public int ticketmachine;
    private int waitpoint;
    private int train;
    private int benchpoint;
    public bool firstInLine;
    private bool gotTicket;
    private bool isOnPhone;
    private bool evacuate;
    private bool prepared;
    private bool decrementDirection;
    private float leftFootPrev;
    private float leftFootAct;
    private float rightFootPrev;
    private float rightFootAct;
    private int shop;
    private int shopPoint;
    public AudioClip[] m_FootstepSounds;
    private AudioSource m_AudioSource;
    private bool dir;
    private bool isMakingAction;
    private Transform rotPoint;

    private float timeToEvacuate;

    public enum State{idle, walk, go_to_line, queue, move_in_queue, get_ticket, go_to_waitzone,
        leave, entering_train, waiting_train, inside_train, inside_moving_train, leaving_train, evacuate, go_to_bench,
        sitting, rotate, activate, go_to_shop, buying, go_to_pay, paying};
	public State state;
	// Use this for initialization

	bool RandomPoint(Vector3 center, float range, out Vector3 result) {
		for (int i = 0; i < 30; i++) {
			Vector3 randomPoint = center + Random.insideUnitSphere * range;
			NavMeshHit hit;
			if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, 5)) {
                NavMeshPath path = new NavMeshPath();
                agent.CalculatePath(hit.position, path);
                if (path.status == NavMeshPathStatus.PathComplete)
                {
                    result = hit.position;
                    return true;
                }
            }
		}
		result = Vector3.zero;
		return false;
	}

	private void GetNewDestination()
	{
		gotDestination = false;
		while (!gotDestination)
			gotDestination=RandomPoint(transform.position, 10.0f, out destination);
        agent.ResetPath();
		agent.destination = destination;
	}

	void Awake () {
		agent = GetComponent<NavMeshAgent> ();
        anim = GetComponent<Animator>();
        m_AudioSource = GetComponent<AudioSource>();
        GetNewDestination();
        if (agent.destination.y > 1.0f) Destroy(this.gameObject);
        timetoidle = 5.0f;
        timetoaction = TIMETOACTION;
		timetogetticket = TICKETTIME;
        timetobuy = BUYTIME;
        timesitting = Random.Range(10.0f,20.0f);
        timeToEvacuate = Random.Range(0f, 6f);
		state = State.walk;
        isOnPhone = false;
        firstInLine = false;
        agent.speed = Random.Range(0.3f, 0.5f);
        agent.areaMask = 37;    //first we set as if it's inside (because inside state in MovementAI is set initially at true)
        gotTicket = false;
        evacuate = false;
        prepared = false;
        decrementDirection = false;
        ticketmachine = -1;
        benchpoint = -1;
        shop = -1;
        shopPoint = -1;
    }

	void CalculateNewState()
	{
		Vector3 pos = new Vector3();
        int k = Random.Range (1, 8);

        switch (k)
        {
            case 1:
                state = State.walk;
                agent.speed = Random.Range(0.3f,0.5f);
                agent.autoBraking = false;
                GetNewDestination();
                break;
            case 2:
                state = State.idle;
                isOnPhone = (Random.value <= ISONPHONEPERCENTAGE);
                if (isOnPhone)
                {
                    anim.SetInteger("State", 1);
                    timetoidle = 49.0f;
                }
                else
                {
                    anim.SetInteger("State", 0);
                    timetoidle = Random.Range(2.0f, 5.0f);
                }
                agent.speed = 0.0f;
                break;
            case 3:
                if (!gotTicket)
                {
                    if (GameManager.atLeastOneTicketMachineFree())
                    {
                        state = State.go_to_line;
                        agent.autoBraking = true;
                        destination = GameManager.getTicketMachinePosition(out ticketmachine).position;
                        agent.speed = Random.Range(0.3f, 0.5f);
                        agent.ResetPath();
                        agent.destination = destination;
                    }
                    else
                    {
                        state = State.walk;
                        agent.speed = Random.Range(0.3f, 0.5f);
                        agent.autoBraking = false;
                        GetNewDestination();
                    }
                }
                else
                {
                    state = State.walk;
                    agent.speed = Random.Range(0.3f, 0.5f);
                    agent.autoBraking = false;
                    GetNewDestination();
                }
                break;
            case 4:
                state = State.leave;
                agent.speed = Random.Range(0.3f, 0.5f);
                pos = GameManager.getNearestExitPoint(transform.position);
                destination = pos;
                agent.ResetPath();
                agent.destination = destination;
                break;
            case 5:
                if (gotTicket)
                {
                    if (GameManager.someWaitzoneHasSpace())
                    {
                        state = State.go_to_waitzone;
                        agent.speed = Random.Range(0.3f, 0.5f);
                        agent.autoBraking = true;
                        destination = GameManager.getWaitpointPosition(out waitpoint, out train);
                        agent.ResetPath();
                        agent.destination = destination;
                    }
                    else
                    {
                        state = State.walk;
                        agent.speed = Random.Range(0.3f, 0.5f);
                        agent.autoBraking = false;
                        GetNewDestination();
                    }
                }
                else
                {
                    state = State.walk;
                    agent.speed = Random.Range(0.3f, 0.5f);
                    agent.autoBraking = false;
                    GetNewDestination();
                }
                break;
            case 6:
                if (GameManager.anyFreeBench(out pos, out benchpoint))
                {
                    state = State.go_to_bench;
                    agent.speed = Random.Range(0.3f, 0.5f);
                    destination = pos;
                    agent.ResetPath();
                    agent.destination = pos;
                }
                else
                {
                    state = State.walk;
                    agent.speed = Random.Range(0.3f, 0.5f);
                    agent.autoBraking = false;
                    GetNewDestination();
                }
                break;
            case 7:
                if (GameManager.anyShopFree(out pos, out shop, out shopPoint))
                {
                    state = State.go_to_shop;
                    agent.speed = Random.Range(0.3f, 0.5f);
                    destination = pos;
                    agent.ResetPath();
                    agent.destination = pos;
                }
                else
                {
                    state = State.walk;
                    agent.speed = Random.Range(0.3f, 0.5f);
                    agent.autoBraking = false;
                    GetNewDestination();
                }
                break;
        }
	}
	
	// Update is called once per frame
	void Update () {
        CheckFootstep();
        if (evacuate && !prepared) {
            if (state != State.inside_train && state != State.inside_moving_train && state != State.sitting && state != State.activate)
            {
                anim.SetInteger("State", 0);
                destination = GameManager.GetRandomExit();
                NavMeshHit hit;
                if (NavMesh.SamplePosition(destination, out hit, 0.2f, agent.areaMask))
                {
                    agent.ResetPath();
                    agent.destination = hit.position;
                    agent.speed = 1.0f;
                    state = State.evacuate;
                    prepared = true;
                }
            }
            if (state == State.sitting) timesitting = 0.0f;
        }

        if (timetoaction < TIMETOACTION) timetoaction += Time.deltaTime;
        if (decrementDirection)
        {
            float d = anim.GetFloat("Direction");
            float d2 = d - d * Time.deltaTime * 2f;
            if ((d < 0 && d2 > 0) || (d > 0 && d2 < 0) || (d2 > -0.1f && d2 < 0) || (d2 < 0.1f && d2 > 0))
            {
                anim.SetFloat("Direction", 0f);
                decrementDirection = false;
            }
            else
            {
                anim.SetFloat("Direction", d2);
            }
        }
        
        switch (state)
        {
            case (State.walk):
                if (Vector3.Distance(transform.position, destination) < 0.2f)
                {
                    agent.areaMask = (((5 | (agent.areaMask & 8)) | (agent.areaMask & 16)) | (agent.areaMask & 32));
                    agent.autoBraking = false;
                    CalculateNewState();
                }
                    
                break;

            case (State.idle):
                timetoidle -= Time.deltaTime;
                if (timetoidle <= 0.0)
                {
                    if (isOnPhone)
                    {
                        anim.SetInteger("State", 0);
                    }
                    CalculateNewState();
                }
                break;

            case (State.go_to_line):
                if (Vector3.Distance(transform.position, destination) < 1.5f)
                {
                    destination = GameManager.getQueuePosition(ticketmachine).position;
                    agent.ResetPath();
                    agent.destination = destination;
                    GameManager.queueCharacter(this.gameObject);
                    state = State.queue;
                }
                break;
            case (State.queue):
                if (Vector3.Distance(transform.position, destination) < 0.3f)
                {
                    agent.ResetPath();
                    dir = GameManager.getDirection(transform,ticketmachine);
                    state = State.rotate;
                }
                break;
            case (State.get_ticket):
                if (firstInLine)
                {
                    if (isMakingAction)
                    {
                        if (anim.GetInteger("Action") == 0)
                        {
                            isMakingAction = false;
                            anim.SetInteger("State", 0);
                        }
                    }

                    if (timetogetticket <= 0.0 && !isMakingAction)
                    {
                        timetogetticket = TICKETTIME;
                        GameManager.unqueueCharacter(ticketmachine);
                        ticketmachine = -1;
                    }
                    
                    else timetogetticket -= Time.deltaTime;
                }
                break;

            case (State.go_to_waitzone):
                if (Vector3.Distance(transform.position, destination) < 0.4f)
                {
                    agent.areaMask = ((13 | (agent.areaMask & 16)) | (agent.areaMask & 32));
                    agent.ResetPath();
                    state = State.waiting_train;
                }
                break;
            case (State.waiting_train):
                //check if the train is in the station
                if (GameManager.isTrainWaiting(train))
                {
                    //check if there's no leaving people
                    if (GameManager.noPeopleLeaving(train) && GameManager.hasEnoughSpace(train))
                    {
                        GameManager.addEnteringTrain(train);
                        destination = GameManager.getTrainDestination(train, transform.position);
                        agent.ResetPath();
                        agent.destination = destination;
                        GameManager.subCharacter(train, waitpoint);
                        state = State.entering_train;
                    }
                }
                break;
            case (State.entering_train):
                if (Vector3.Distance(transform.position, destination) <= agent.stoppingDistance)
                {
                    agent.stoppingDistance = STOPPINGDIST;
                    agent.ResetPath();
                    GameManager.addCharacterToTrain(train,gameObject);
                    state = State.inside_train;
                }
                else if (Vector3.Distance(transform.position, destination) < 4.0f)
                {
                        agent.stoppingDistance += 0.01f;
                }
                break;
            case (State.inside_train):
                if (!GameManager.isTrainWaiting(train))
                {
                    state = State.inside_moving_train;
                }
                break;
            case (State.inside_moving_train):
                if (GameManager.isTrainWaiting(train))
                {
                    //reset all the data and leave the train
                    destination = GameManager.getNearestWaitpoint(train, gameObject.transform.position);
                    agent.ResetPath();
                    agent.destination = destination;
                    agent.speed = Random.Range(0.3f, 0.5f);
                    agent.avoidancePriority = 20;
                    gotTicket = false;
                    state = State.leaving_train;
                }
                break;
            case (State.leaving_train):
                if (Vector3.Distance(transform.position, destination) <= 0.3f)
                {
                    agent.areaMask = ((5 | (agent.areaMask & 16)) | (agent.areaMask & 32));
                    agent.avoidancePriority = 50;
                    GameManager.subCharacterLeaving(train);
                    destination = GameManager.findNearestTrainPoint(transform.position);
                    agent.ResetPath();
                    agent.destination = destination;
                    state = State.walk;
                }
                break;
            case (State.move_in_queue):
                if (Vector3.Distance(transform.position, destination) < 0.3f)
                {
                    agent.speed = 0.0f;
                    dir = GameManager.getDirection(transform, ticketmachine);
                    state = State.rotate;
                }
                break;
            case (State.evacuate):
                if (Vector3.Distance(transform.position, destination) < 0.5f)
                {
                    Destroy(this.gameObject);
                }
                break;
            case (State.leave):

                if (Vector3.Distance(transform.position, destination) < 1.0f)
                {
                    GameManager.subCharacterInScene();
                    Destroy(this.gameObject);
                }
                break;
            case (State.go_to_bench):
                if (Vector3.Distance(transform.position, destination) <= 0.2f)
                {
                    agent.ResetPath();
                    anim.SetFloat("Velocity", 0.0f);
                    rotPoint = GameManager.getBenchPoint(benchpoint);
                    dir = GameManager.getDirection(transform, rotPoint);
                    /*transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY
                        | RigidbodyConstraints.FreezePositionZ;*/
                    //GetComponent<NavMeshObstacle>().enabled = true;
                    //GetComponent<NavMeshObstacle>().radius = 0.4f;
                    agent.enabled = false;
                    state = State.rotate;
                }
                break;
            case (State.rotate):
                if (ticketmachine != -1)
                {
                    if (GameManager.isAlignedWith(transform, ticketmachine, "ticket"))
                    {
                        if (firstInLine)
                        {
                            anim.SetFloat("Direction", 0.0f);
                            isMakingAction = true;
                            anim.SetInteger("State", 2);
                            anim.SetInteger("Action", 2);
                        }
                        state = State.get_ticket;
                    }
                    else rotateCharacter();
                }
                else
                {
                    if (GameManager.isAlignedWith(transform, rotPoint))
                    {
                        if (shopPoint != -1)
                        {
                            isMakingAction = true;
                            anim.SetInteger("State", 2);
                            anim.SetInteger("Action", 2);
                            state = State.buying;
                        }
                        else if (shop != -1)
                        {
                            isMakingAction = true;
                            anim.SetInteger("State", 2);
                            anim.SetInteger("Action", 2);
                            state = State.paying;
                        }
                        else if (benchpoint != -1)
                        {
                            anim.SetInteger("State", 3);
                            state = State.sitting;
                        }
                        decrementDirection = true;
                    }
                    else rotateCharacter();
                }
                break;
            case (State.sitting):
                if (timesitting > 0.0f) timesitting -= Time.deltaTime;
                else
                {
                    GameManager.emptyBench(benchpoint);
                    benchpoint = -1;
                    timesitting = Random.Range(10.0f,20.0f);
                    anim.SetInteger("State",0);
                    state = State.activate;
                }
                break;
            case (State.activate):
                if (agent.enabled == true && anim.IsInTransition(0) && anim.GetNextAnimatorStateInfo(0).IsName("IdleWalk"))
                {
                    if (evacuate)
                    {
                        anim.SetInteger("State", 0);
                        destination = GameManager.getNearestExitPoint(transform.position);
                        NavMeshHit hit;
                        if (NavMesh.SamplePosition(destination, out hit, 0.2f, agent.areaMask))
                        {
                            agent.ResetPath();
                            agent.destination = hit.position;
                            agent.speed = 1.0f;
                            state = State.evacuate;
                            prepared = true;
                        }
                    }
                    else CalculateNewState();
                }
                break;
            case (State.go_to_shop):
                if (Vector3.Distance(transform.position, destination) <= 0.2f)
                {
                    agent.ResetPath();
                    rotPoint = GameManager.getShopPoint(shop, shopPoint);
                    dir = GameManager.getDirection(transform, rotPoint);
                    state = State.rotate;
                }
                break;
            case (State.buying):
                if (isMakingAction)
                {
                    if (anim.GetInteger("Action") == 0)
                    {
                        isMakingAction = false;
                        anim.SetInteger("State", 0);
                    }
                }
                Vector3 nextpos = new Vector3();
                if (timetobuy <= 0.0 && !isMakingAction && !GameManager.isBuypointOccupied(out nextpos, shop))
                {
                    GameManager.setShopPoint(shop, shopPoint, false);
                    shopPoint = -1;
                    destination = nextpos;
                    agent.ResetPath();
                    agent.destination = destination;
                    timetobuy = BUYTIME;
                    state = State.go_to_pay;
                }
                else timetobuy -= Time.deltaTime;

                break;
            case (State.go_to_pay):
                if (Vector3.Distance(transform.position, destination) <= 0.2f)
                {
                    agent.ResetPath();
                    rotPoint = GameManager.getBuyPoint(shop);
                    dir = GameManager.getDirection(transform, rotPoint);
                    state = State.rotate;
                }
                break;
            case (State.paying):
                if (isMakingAction)
                {
                    if (anim.GetInteger("Action") == 0)
                    {
                        isMakingAction = false;
                        anim.SetInteger("State", 0);
                    }
                }

                if (timetobuy <= 0.0 && !isMakingAction)
                {
                    timetobuy = BUYTIME;
                    GameManager.setShopPoint(shop, false);
                    shop = -1;
                    state = State.walk;
                    agent.speed = Random.Range(0.3f,0.5f);
                    GetNewDestination();
                }
                else timetobuy -= Time.deltaTime;

                break;
        }
    }

    public void setDestination(Vector3 p)
    {
        destination = p;
        agent.destination = p;
    }

    public void setFirstInLine(bool t)
    {
        firstInLine = t;
    }

    public void setgotTicket()
    {
        state = State.walk;
        gotTicket = true;
        agent.speed = Random.Range(0.3f, 0.5f);
        GetNewDestination();
    }

    public void moveInqueue()
    {
        state = State.move_in_queue;
        agent.speed = 0.3f;
    }

    public void setTiquetMachine(int k)
    {
        ticketmachine = k;
    }

    public int getTicketmachine()
    {
        return ticketmachine;
    }

    public State getActualState()
    {
        return state;
    }

    private void Evacuate()
    {
        evacuate = true;
    }

    public void setEvacuate()
    {
        Invoke("Evacuate", timeToEvacuate); 
    }

    private void CheckFootstep()
    {
        leftFootAct = anim.GetFloat("LeftFoot");
        rightFootAct = anim.GetFloat("RightFoot");
        //check left foot frame
        if (leftFootAct >= 0 && leftFootPrev < 0) footstep();
        if (rightFootAct <= 0 && rightFootPrev > 0) footstep();
        leftFootPrev = anim.GetFloat("LeftFoot");
        rightFootPrev = anim.GetFloat("RightFoot");
    }

    private void footstep()
    {
        int n = Random.Range(0, m_FootstepSounds.Length);
        m_AudioSource.clip = m_FootstepSounds[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
    }

    private void rotateCharacter()
    {
        if (!dir)
        {
            anim.SetFloat("Direction", 0.4f);
            transform.Rotate(new Vector3(0.0f, 60.0f * Time.deltaTime, 0.0f));
        }
        else
        {
            anim.SetFloat("Direction", -0.4f);
            transform.Rotate(new Vector3(0.0f, -60.0f * Time.deltaTime, 0.0f));
        }
    }

    public void resetAction()
    {
        anim.SetInteger("Action", 0);
    }

    public void disableObstacle()
    {
        GetComponent<NavMeshObstacle>().enabled = false;
    }

    public void enableAgent()
    {
        agent.enabled = true;
        agent.ResetPath();
    }
}