﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class sellerBehaviour : MonoBehaviour {

    private Animator anim;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        anim.SetFloat("Idle1", 1.0f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void resetAction()
    {
        anim.SetInteger("Action", 0);
    }
}
