﻿using UnityEngine;
using System.Collections;

public class CharacterSpawner : MonoBehaviour {

	public int NumCharacters;
    public int totalChars;
	public GameObject[] listOfCharacters;
    public float generationTime=0;
    public GameObject[] exitPoints;
    // Use this for initialization

    private GameObject d;

    bool RandomPoint(Vector3 center, float range, out Vector3 result) {
		for (int i = 0; i < 30; i++) {
			Vector3 randomPoint = center + Random.insideUnitSphere * range;
            
			UnityEngine.AI.NavMeshHit hit;
			if (UnityEngine.AI.NavMesh.SamplePosition(randomPoint, out hit, 1.0f, 5)) {
                result = hit.position;
                if (hit.position.y > 1.0f)
                    return false;
                return true;                                  
			}
		}
		result = Vector3.zero;
		return false;
	}

	private Vector3 GetRandomPoint()
	{
		Vector3 point=Vector3.zero;
		bool gotDestination = false;
		while (gotDestination==false)
			gotDestination=RandomPoint(transform.position, 20.0f, out point);
		return point;
	}

	void Start () {

        d = GameObject.Find("data");

        if (d != null)
        {
            totalChars = d.GetComponent<data>().chars;
            NumCharacters = totalChars / 10;
            generationTime = Random.Range(d.GetComponent<data>().genTime - 1.0f, d.GetComponent<data>().genTime + 1.0f);
        }
        else
        {
            generationTime = Random.Range(generationTime - 5f, generationTime + 5f);
        }
		for (int i = 0; i < NumCharacters; i++) {
            IniCharacter();
		}
        if (generationTime>0.0f) InvokeRepeating("GenerateCharacter", 0.0f, generationTime);
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void GenerateCharacter()
    {
        //Get number of characters in scene
        //If number is greater or equal than numcharacters, do nothing
        //else generate new character on the exit point (where the user cannot see the spawn of the character)
        if (GameManager.getCharactersInScene() < totalChars)
        {
            int i = Random.Range(0,exitPoints.Length);
            Vector3 p = exitPoints[i].transform.position;
            GameObject pers = Instantiate(listOfCharacters[Random.Range(0, listOfCharacters.Length)], p, this.transform.rotation) as GameObject;
            GameManager.addCharacterToScene();
        }

    }

    void IniCharacter()
    {
        Vector3 p;
        p = GetRandomPoint();
        GameObject pers = Instantiate(listOfCharacters[Random.Range(0, listOfCharacters.Length)], p, this.transform.rotation) as GameObject;
        GameManager.addCharacterToScene();
    }

    public void cancelGeneration()
    {
        CancelInvoke("GenerateCharacter");
    }
}
