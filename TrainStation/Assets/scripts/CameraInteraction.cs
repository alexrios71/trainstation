﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInteraction : MonoBehaviour {

    public GameObject player;
    //private Animator anim;
    private bool isMakingAction;
    private string[] interactables = { "toilet", "book", "shoes", "shirts", "fruit", "tool", "furniture", "ball",
        "bicycle", "racket", "vrh", "fridge", "furnace", "microwave", "printer", "speakers", "tv", "pc", "drink",
        "cashmachine", "vendingticket" };
    //private string[] listToDo = { "toilet", "drink", "book", "ball", "vendingticket" };
    private int taskNumber;
    private bool[] hasItem;
    private GameObject lastObj;

	// Use this for initialization
	void Start () {
        lastObj = null;
        taskNumber = 0;
        //anim = player.GetComponent<Animator>();
        hasItem = new bool[interactables.Length];
        for (int i = 0; i < hasItem.Length; ++i) hasItem[i] = false;
	}
	
	// Update is called once per frame
	void Update () {

        /*if (isMakingAction)
        {
            if (anim.GetInteger("Action") == 0)
            {
                isMakingAction = false;
                anim.SetInteger("State", 0);
                anim.speed = 1.0f;
                //player.GetComponent<MovementPlayer>().enableRotation();
            }
        }*/

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 3.0f))
        {
            //Vector3 pos = transform.position + new Vector3()*transform.forward;
            if (Input.GetMouseButtonDown(0))
            {
                string thing = hit.transform.tag;
                if (thing == "door")
                {
                    if (hit.transform.GetComponent<Animator>().GetInteger("State") == 0)
                    {
                        hit.transform.GetComponent<Animator>().SetInteger("State", 1);
                    }
                    else
                    {
                        hit.transform.GetComponent<Animator>().SetInteger("State", 0);
                    }
                }
                else if (taskNumber < 5)
                {
                    if ((thing == "cashmachine" || thing == "vendingticket") && itemInInventory(thing) && hit.distance < 0.7f)
                    {
                        if (Mathf.Abs(hit.transform.eulerAngles.y - player.transform.eulerAngles.y) > 80.0f &&
                            Mathf.Abs(hit.transform.eulerAngles.y - player.transform.eulerAngles.y) < 100.0f)
                        {
                            //isMakingAction = true;
                            //anim.SetInteger("State", 2);
                            //anim.SetInteger("Action", 6);
                            //anim.speed = 2.0f;

                            saveActionData(thing);
                            //player.GetComponent<MovementPlayer>().disableRotation();
                        }
                    }
                    else if (thing == "toilet" && !hasItem[0] && hit.distance < 2.5f)
                    {
                        hasItem[0] = true;
                        GameManager.activateAudio(hit.transform, "toilet");
                        saveActionData(thing);
                    }
                    else if (thing != "Untagged" && itemInInventory(thing) && hit.distance < 1.5f)
                    {
                        
                        /*isMakingAction = true;
                        anim.SetInteger("State", 2);
                        anim.SetInteger("Action", 6);
                        anim.speed = 2.0f;*/
                        saveActionData(thing);
                        //player.GetComponent<MovementPlayer>().disableRotation();
                    }
                }
            }
            if (hit.transform.gameObject.layer == 12)
            {
                lastObj = hit.transform.gameObject;
                lastObj.GetComponent<HighlightObject>().setHighlight(true);
            }
            else if (lastObj != null)
            {
                lastObj.GetComponent<HighlightObject>().setHighlight(false);
                lastObj = null;
            }
        }
    }

    private bool itemInInventory(string item)
    {
        for (int i = 0; i < interactables.Length; ++i)
        {
            if (item == interactables[i])
            {
                if (!hasItem[i])
                {
                    hasItem[i] = true;
                    return true;
                }
                else return false;
            }
        }
        return false;
    }

    private void saveActionData(string thing)
    {
        GameManager.AddAction(thing);
        if (taskNumber == 0) GameManager.StartCountdown();
        else if (taskNumber == 2) GameManager.StartEvacuating();
        ++taskNumber;
    }
}
