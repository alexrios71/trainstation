﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class simulationStart : MonoBehaviour {

    //public Canvas[] canv;
    public int percEvac;
    public int evacuationTimeSeconds;
    public int executionID;
    public bool fireON;
    public InputField seconds;
    public InputField percEv;
    public InputField ID;
    public Toggle fire;
	public Text ErrorMSG;

    private bool done;
    private bool start;

	// Use this for initialization
	void Start () {
        start = true;
        done = false;
      //  DontDestroyOnLoad(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
        /*if (!done)
        {
            if (Input.GetKeyDown(KeyCode.Keypad1)) startSim(0);
            else if (Input.GetKeyDown(KeyCode.Keypad2)) startSim(25);
            else if (Input.GetKeyDown(KeyCode.Keypad3)) startSim(50);
            else if (Input.GetKeyDown(KeyCode.Keypad4)) startSim(75);
        }
        
        if (start)
        {
            StartCoroutine(countdown());
            start = false;
        }*/
    }

    private void startSim(int p)
    {
        if (seconds.text != "" && int.Parse(seconds.text) > 0)
        {
            evacuationTimeSeconds = int.Parse(seconds.text);
            percEvac = p;
            start = true;
            done = true;
        }
    }

    public void buttonClick()
    {
		ErrorMSG.text = "";
		if ((ID.text == "") || (int.Parse(ID.text)<0))
			ErrorMSG.text = "ID en format incorrecte: "+ID.text;
		else if ((percEv.text == "") || (int.Parse(percEv.text)<0) || int.Parse(percEv.text)>100)
			ErrorMSG.text = "Percentatge en format incorrecte: "+percEv.text;
		else if ((seconds.text == "") || (int.Parse(seconds.text)<0) || int.Parse(seconds.text)>120)
			ErrorMSG.text = "Segons en format incorrecte: "+seconds.text;
        /* if (start)
         {
             Debug.Log("entra");*/
		/*if (seconds.text.ToString() == "" || int.Parse (seconds.text.ToString()) < 0)
			ErrorMSG.text = "Segons en format incorrecte: "+seconds.text.ToString();
		else if (percEv.text.ToString() == "" || (int.Parse(percEv.text.ToString()) < 0 || int.Parse(percEv.text.ToString())>100))
			ErrorMSG.text = "Percentatge en format incorrecte: "+percEv.text.ToString();
		else if (ID.text.ToString() == "" || int.Parse(ID.text.ToString())<=0)
			ErrorMSG.text = "Percentatge en format incorrecte: "+ID.text.ToString();
		else{
            evacuationTimeSeconds = int.Parse(seconds.text);
            percEvac = int.Parse(percEv.text);
            executionID = int.Parse(ID.text);
            fireON = fire.isOn;

            /******************************/

            PlayerPrefs.SetInt("evacuationTime", int.Parse(seconds.text));
            PlayerPrefs.SetInt("percentage", int.Parse(percEv.text));
            PlayerPrefs.SetInt("executionID", int.Parse(ID.text));
            PlayerPrefs.SetInt("fireOn", fire.isOn ? 1 : 0);
        
            StartCoroutine(countdown());
            // start = false;
        

    }

    private IEnumerator countdown()
    {
        float fadeTime = GetComponent<Fade>().startFade(1);
        yield return new WaitForSeconds(1/fadeTime);
        StopCoroutine(countdown());
        SceneManager.LoadScene("scene5");
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void LoadSimulation()
    {
        SceneManager.LoadScene("data");
    }
}
