﻿using UnityEngine;
using System.Collections;

public class VendingMachine : MonoBehaviour {

    public float distanceBetweenAgents;
    public Vector3 queueDirection;
    private ArrayList listOfCharacters;
    private ArrayList positions;
    private int charsinqueue;

    private int posChar;
    // Use this for initialization
    void Start () {
        posChar = 0;
        listOfCharacters = new ArrayList();
        positions = new ArrayList();
        charsinqueue = 0;
    }

    void Update()
    {
    }
	
    public Transform getNextPosition()
    {
        Transform[] s = GetComponentsInChildren<Transform>();
        GameObject t = new GameObject();
        positions.Add(s[posChar+1]);
        t.transform.parent = this.transform;
        t.transform.position = new Vector3(s[posChar + 1].transform.position.x, s[posChar + 1].transform.position.y, s[posChar + 1].transform.position.z) + queueDirection * distanceBetweenAgents;

        posChar++;
        return s[posChar];
    }

    public void AddCharacter(GameObject a)
    {
        if (listOfCharacters.Count == 0)
            a.GetComponent<CharacterControllerPHD>().setFirstInLine(true);
        listOfCharacters.Add(a);
    }

    public void UnqueueCharacter()
    {
        if (listOfCharacters.Count > 0)
        {
            
            GameObject g;
            g = listOfCharacters[0] as GameObject;
            g.GetComponent<CharacterControllerPHD>().setFirstInLine(false);
            //g.GetComponent<CharacterControllerPHD>().setDestination(GameObject.Find("trainPoint").transform.position);
            g.GetComponent<CharacterControllerPHD>().setgotTicket();
            listOfCharacters.RemoveAt(0);
            if (listOfCharacters.Count > 0)
            {
                g = listOfCharacters[0] as GameObject;
                g.GetComponent<CharacterControllerPHD>().setFirstInLine(true);
                for(int k=0; k<listOfCharacters.Count;k++)
                {
                   
                    Vector3 p = (positions[k] as Transform).position;
                    g = listOfCharacters[k] as GameObject;
                    g.GetComponent<CharacterControllerPHD>().setDestination(p);
                    g.GetComponent<CharacterControllerPHD>().moveInqueue();
                }
            }
            positions.RemoveAt(positions.Count-1);
            Destroy(this.GetComponentsInChildren<Transform>()[this.GetComponentsInChildren<Transform>().Length - 1].gameObject);
            posChar--;
            charsinqueue--;
        }
    }

    public bool haySitio()
    {
        return (charsinqueue < 5);
    }

    public void AddCharacter()
    {
        charsinqueue++;
    }

    public int getCharsInQueue()
    {
        return charsinqueue;
    }
}
