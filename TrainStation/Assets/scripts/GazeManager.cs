﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System;

using UnityEngine;

public class GazeManager : MonoBehaviour
{
	public FoveInterface foveInterface;
	public List<Collider> colliderList;
    public string filePath;

    private string dataEntry;
    private StreamWriter sw;
    private Collider currentCollider, oldCollider;
    private float startLookingTime;
	// Use this for initialization
	void Start()
	{
		oldCollider = null;
        currentCollider = null;
        string fn;
        if (PlayerPrefs.GetInt("fireOn") == 1)
        {
            fn = "F";
        }
        else
        {
            fn = "NF";
        }
        fn += PlayerPrefs.GetInt("percentage");
        fn += "-"+PlayerPrefs.GetInt("executionID");
        filePath += "\\" + fn + "_GazeData.csv";
        sw = new StreamWriter(filePath);
}

// Update is called once per frame
void Update()
	{
		//Debug.Log(LayerMask.NameToLayer("GazeLayer"));
//		if (foveInterface.Gazecast(LayerMask.NameToLayer("GazeLayer"), out which)) {
		//foveInterface.Gazecast(LayerMask.NameToLayer("GazeLayer"), out which);
		if (foveInterface.Gazecast(colliderList, out currentCollider))
        {
            if (currentCollider != oldCollider)
            {
                startLookingTime = Time.timeSinceLevelLoad;
                //UnityEngine.Debug.Log(currentCollider.gameObject.name + startLookingTime + " look in");// start looking
                oldCollider = currentCollider;
                dataEntry = currentCollider.gameObject.name + "; " + startLookingTime + "; ";
            }
        }
        else
        {
            if (currentCollider != oldCollider)
            {
                float t = Time.timeSinceLevelLoad;
                float deltaT = t - startLookingTime;
                dataEntry += t + "; " + deltaT;
                sw.WriteLine(dataEntry);
                sw.Flush();
                //UnityEngine.Debug.Log(oldCollider.gameObject.name + t + " look out");// stop looking
                //UnityEngine.Debug.Log("Total time" + deltaT);
                UnityEngine.Debug.Log(dataEntry);
                oldCollider = null;
            }
        }
    }

    private void OnApplicationQuit()
    {
            sw.Close();
    }
}
