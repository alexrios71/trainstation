﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using UnityStandardAssets.Characters.FirstPerson;
using Random = UnityEngine.Random;
using Valve.VR;
using GamepadInput;


//[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(AudioSource))]
public class MovementPlayer : MonoBehaviour
{
    public enum ControlMode {GamePad, HeadSet, Strafe };
    public ControlMode controlMode;

    public float avatarScale;

    private const float YMAX = 0.15f;
    private const float ZMAX = 0.2f;
    private const float CAMERAPOSX = -0.013f;
    private const float CAMERAPOSY = 1.575f;
    private const float CAMERAPOSZ = 0.101f;

   // [SerializeField]
   // private float maxSpeed;
    [SerializeField]
    private MouseLook m_MouseLook;
    [SerializeField]
    private float m_StepInterval;
    [SerializeField]
    private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.

    private Animator anim;
    private Rigidbody rb;
    private Camera m_Camera;
    private AudioSource m_AudioSource;
    private float actualDirection;
    private float actualVelocity;
    private float tVel;
    private float tDir;
    private float tCam;
    private Vector3 posCamera;
    private float leftFootPrev;
    private float leftFootAct;
    private float rightFootPrev;
    private float rightFootAct;
   

    private Vector2 movement;
    public float strafeSpeed, runStrafeSpeed, speed, angleSpeed, runSpeed, turnSpeed;
    public float strafeSpeed_, speed_, angleSpeed_;

    public float radiusRot;
    public GameObject cameraParent;
   
    public Transform leftHand;
    public Transform rightHand;

    private bool lookAway;
    
    

    // Use this for initialization
    private void Start()
    {
        
        rb = GetComponent<Rigidbody>();
        m_Camera = Camera.main;
        m_AudioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        anim.SetFloat("Idle1", 1.0f);
        actualVelocity = 0.0f;
        actualDirection = 0.0f;
        tVel = 0.0f;
        tDir = 0.5f;
        tCam = 0.0f;
        posCamera = cameraParent.transform.localPosition;
        lookAway = false;
        
    }


    // Update is called once per frame
    private void Update()
    {
        // Scale the avatar using keyboard
        if (Input.GetKey(KeyCode.O))
        {
            avatarScale += 0.1f * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.L))
        {
            avatarScale -= 0.1f * Time.deltaTime;
        }
        transform.localScale = new Vector3 (avatarScale, avatarScale, avatarScale);
        
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        handleJoystick();

        //updatePlayer();
        Debug.Log(actualVelocity);
        anim.SetFloat("Velocity", actualVelocity);
       
        CheckFootstep();
    }
    private void handleJoystick()
    {
        
        speed_ = speed;
        angleSpeed_ = angleSpeed;
        strafeSpeed_ = strafeSpeed;
        if (Input.GetButton("TriggersL_0")|| Input.GetButton("TriggersL_1"))
        {
            speed_ = runSpeed;
            angleSpeed_ = angleSpeed+turnSpeed;
            strafeSpeed_ = runStrafeSpeed;
        }
        //movement = new Vector2(Input.GetAxisRaw("L_XAxis_3"), Input.GetAxisRaw("L_YAxis_3"));

        movement = GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.One);
        //movement = GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Two);
        //movement = GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Three);
        //movement = GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Four);

        //Debug.Log("movement:" + movement);
        if (movement.y > 0.0f)
        {
            movement.y = 1f;
        }else if (movement.y < 0.0f)
        {
            movement.y = -1f;
        }
        actualVelocity = movement.y * speed_;
    }

    private void CheckFootstep()
    {
        leftFootAct = anim.GetFloat("LeftFoot");
        rightFootAct = anim.GetFloat("RightFoot");
        //check left foot frame
        if (leftFootAct >= 0 && leftFootPrev < 0) footstep();
        if (rightFootAct <= 0 && rightFootPrev > 0) footstep();
        leftFootPrev = anim.GetFloat("LeftFoot");
        rightFootPrev = anim.GetFloat("RightFoot");
    }

    private void footstep()
    {
        int n = Random.Range(0, m_FootstepSounds.Length);
        m_AudioSource.clip = m_FootstepSounds[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
    }

    public void resetAction()
    {
        anim.SetInteger("Action", 0);
    }

    //private void updatePlayer()
    void LateUpdate()
    {

        float desiredRot;               // the rotation angle of the camera.
        Quaternion desiredRotQ;         // the desired rotation of the body.
        Vector3 bodyVec = new Vector3(transform.forward.x, 0.0f, transform.forward.z); // A vector for the body orientation neglecting the Y dimension
        Vector3 camVec = new Vector3(cameraParent.transform.forward.x, 0.0f, cameraParent.transform.forward.z); // A vector for the camera orientation neglecting the Y dimension

        Vector3 translation;

        switch (controlMode)
        {
            case ControlMode.GamePad:
                float angleY = Vector3.Angle(bodyVec, camVec);  // Computes the angle between the camera and the body.
                if (angleY >= 60)
                {
                    if (Vector3.Cross(bodyVec, camVec).y > 0.0f)
                    {
                        if (movement.x > 0.0f)
                            transform.Rotate(transform.up, movement.x * Time.deltaTime * angleSpeed_);
                        else
                            transform.Rotate(transform.up, angleY - 60.0f);
                    }
                    else
                    {
                        if (movement.x < 0.0f)
                            transform.Rotate(transform.up, movement.x * Time.deltaTime * angleSpeed_);
                        else
                            transform.Rotate(transform.up, 60.0f - angleY);
                    }
                }
                else
                {
                    transform.Rotate(transform.up, movement.x * Time.deltaTime * angleSpeed_);
                }
                
                if (angleY >= 15)
                {
                    lookAway = true;
                }

                if (lookAway && angleY < 15)
                {
                    desiredRot = cameraParent.transform.eulerAngles.y; 
                    desiredRotQ = Quaternion.Euler(transform.eulerAngles.x, desiredRot, transform.eulerAngles.z);
                    transform.rotation = desiredRotQ;
                    lookAway = false;
                }
                
                break;
            case ControlMode.Strafe:
                desiredRot = cameraParent.transform.eulerAngles.y;
                desiredRotQ = Quaternion.Euler(transform.eulerAngles.x, desiredRot, transform.eulerAngles.z);
                transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotQ, angleSpeed_ * Time.deltaTime);
                translation = transform.right * strafeSpeed_ * movement.x * Time.deltaTime;
                transform.Translate(translation, Space.World);
                //Debug.Log(angleSpeed_ * Time.deltaTime);
                break;
            case ControlMode.HeadSet:
                desiredRot = cameraParent.transform.eulerAngles.y; 
                desiredRotQ = Quaternion.Euler(transform.eulerAngles.x, desiredRot, transform.eulerAngles.z);
                transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotQ, angleSpeed_ * Time.deltaTime);
                //Debug.Log(angleSpeed_ * Time.deltaTime);
                break;
        }
    }
}
