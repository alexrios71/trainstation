﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControllerMovementAI : MonoBehaviour
{
    [SerializeField]
    float m_MovingTurnSpeed = 360;
    [SerializeField]
    float m_StationaryTurnSpeed = 180;

    private NavMeshAgent agent;

    const float MAX_AVOIDANCE_ANGLE = 90.0f;

    Rigidbody rb;
    Animator anim;
    float directionValue;
    float velocityValue;
    private bool isInside;
    private bool avoid;
    private float timerProxy;
    private bool hasExitProxy;
    private List<GameObject> collidersNear;
    private Vector3 direction;

    private void Awake()
    {
        // get the components on the object we need ( should not be null due to require component so no need to check )
        agent = GetComponentInChildren<NavMeshAgent>();
        agent.updateRotation = true;
        agent.updatePosition = true;
        timerProxy = 2.0f;
        isInside = true;
        avoid = false;
        hasExitProxy = false;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        collidersNear = new List<GameObject>();
        direction = new Vector3(0f, 0f, 0f);
    }


    private void Update()
    {
        if (agent.isActiveAndEnabled && agent.remainingDistance > agent.stoppingDistance)
        {
            Move(agent.desiredVelocity);
        }

        else if (agent.isActiveAndEnabled)
            Move(Vector3.zero);
    }

    private void Move(Vector3 move)
    {
        move = DetectCollision(move);
        direction = move;
        move = transform.InverseTransformDirection(move);
        directionValue = Mathf.Atan2(move.x, move.z);
        velocityValue = move.z;
        /*if (avoid) velocityValue = -move.z;
        else if (!hasExitProxy) velocityValue = move.z;
        else
        {
            velocityValue = 0.0f;
            timerProxy -= Time.deltaTime;
            if (timerProxy <= 0)
            {
                timerProxy = 2.0f;
                hasExitProxy = false;
            }
        }*/
        ApplyExtraTurnRotation();
        anim.SetFloat("Velocity", velocityValue, 0.1f, Time.deltaTime);
        anim.SetFloat("Direction", directionValue, 0.1f, Time.deltaTime);
    }

    void ApplyExtraTurnRotation()
    {
        // help the character turn faster (this is in addition to root rotation in the animation)
        float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, velocityValue);
        transform.Rotate(0, directionValue * turnSpeed * Time.deltaTime, 0);
    }

    public void OnAnimatorMove()
    {
        // we implement this function to override the default root motion.
        // this allows us to modify the positional speed before it's applied.
        if (Time.deltaTime > 0)
        {
            Vector3 v = anim.deltaPosition / Time.deltaTime;
            // we preserve the existing y part of the current velocity.
            v.y = rb.velocity.y;
            rb.velocity = v;
        }
    }

    public void setInside(bool b)
    {
        isInside = b;
    }

    public bool getInside()
    {
        return isInside;
    }

    void OnTriggerEnter(Collider other)
    {
        /*if (other.tag == "proxy" && !isInside)
        {
            avoid = true;
        }*/
    }

    void OnTriggerExit(Collider other)
    {
        /*if (other.tag == "proxy" && !isInside && !other.GetComponent<Proxy>().isInsideList(gameObject))
        {
            hasExitProxy = true;
            avoid = false;
        }*/
    }

    private Vector3 DetectCollision(Vector3 move)
    {
        GameObject col = GetNearestCollision();
        Vector3 moveNorm = move;
        if (col != null)
        {
            if (col.transform.parent.tag == "character")
            {
                Vector3 vecColliders = (col.transform.position - transform.position).normalized;
                float angle = Vector3.Angle(vecColliders, moveNorm);
                if (angle < MAX_AVOIDANCE_ANGLE) //needs to avoid if he's going to the obstacle
                {
                    //Get closest angle where the agent has to go (from the two perpendicular vectors
                    //we got from the center of the obstacle and the center of the agent)
                    Vector3 perp1 = new Vector3(-vecColliders.z, 0f, vecColliders.x);   //left perpendicular
                    Vector3 perp2 = new Vector3(vecColliders.z, 0f, -vecColliders.x);   //right perpendicular

                    //Get closest vector to de direction vector
                    if (Vector3.Angle(perp1, moveNorm) < Vector3.Angle(perp2, moveNorm))
                    {
                        string obstacleDir = col.GetComponentInParent<ControllerMovementAI>().getDirection(transform.position);
                        //string obstacleDir = "left";
                        if (obstacleDir == "left")
                        {
                            moveNorm = moveNorm.normalized + perp1 * UnityEngine.Random.Range(1f, 1.2f);
                            moveNorm = moveNorm.normalized * move.magnitude;
                        }
                        else
                        {
                            moveNorm = new Vector3(0f, 0f, 0f);
                        }

                    }
                    else
                    {
                        moveNorm = moveNorm.normalized + perp2 * UnityEngine.Random.Range(1f, 1.2f);
                        moveNorm = moveNorm.normalized * move.magnitude;
                    }
                }
            }
            else if (col.transform.parent.tag == "Player")
            {
                Vector3 vecColliders = (col.transform.position - transform.position).normalized;
                float angle = Vector3.Angle(vecColliders, moveNorm);
                if (angle < MAX_AVOIDANCE_ANGLE) //needs to avoid if he's going to the obstacle
                {
                    //Get closest angle where the agent has to go (from the two perpendicular vectors
                    //we got from the center of the obstacle and the center of the agent)
                    Vector3 perp1 = new Vector3(-vecColliders.z, 0f, vecColliders.x);   //left perpendicular
                    Vector3 perp2 = new Vector3(vecColliders.z, 0f, -vecColliders.x);   //right perpendicular

                    //Get closest vector to de direction vector
                    if (Vector3.Angle(perp1, moveNorm) < Vector3.Angle(perp2, moveNorm))
                    {
                        moveNorm = moveNorm.normalized + perp1 * UnityEngine.Random.Range(1f, 1.2f);
                        moveNorm = moveNorm.normalized * move.magnitude;
                    }
                    else
                    {
                        moveNorm = moveNorm.normalized + perp2 * UnityEngine.Random.Range(1f, 1.2f);
                        moveNorm = moveNorm.normalized * move.magnitude;
                    }
                }
            }
        }
        return moveNorm;
    }

    public bool existsCollider(GameObject col)
    {
        return (collidersNear.Contains(col));
    }

    public void addNewCollider(GameObject col)
    {
        collidersNear.Add(col);
    }

    public void deleteCollider(GameObject col)
    {
        collidersNear.Remove(col);
    }

    private GameObject GetNearestCollision()
    {
        if (collidersNear.Count == 0) return null;
        GameObject res = collidersNear[0];
        int i = 1;
        while (i < collidersNear.Count)
        {
            if (res == null)
            {
                res = collidersNear[i];
                ++i;
            }
            else
            {
                if (collidersNear[i] == null)
                {
                    collidersNear.RemoveAt(i);
                }
                else
                {
                    if (Vector3.Distance(collidersNear[i].transform.position, transform.position) < Vector3.Distance(res.transform.position, transform.position))
                    {
                        res = collidersNear[i];
                    }
                    ++i;
                }
            }
        }
        return res;
    }

    public string getDirection(Vector3 pos)
    {
        Vector3 vecColliders = (pos - transform.position).normalized;
        Vector3 perp1 = new Vector3(-vecColliders.z, 0f, vecColliders.x);   //left perpendicular
        Vector3 perp2 = new Vector3(vecColliders.z, 0f, -vecColliders.x);   //right perpendicular
        if (Vector3.Angle(perp1, direction) <= Vector3.Angle(perp2, direction)) return "left";
        else return "right";
    }
}
