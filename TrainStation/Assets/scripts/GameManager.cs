﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameManager : MonoBehaviour {

    private System.DateTime dateAndTime;
    private const float FRAMERATE = 0.1f;
    private float TIMETOEVACUATE = 120.0f;
    private int execID = 0;

    public GameObject[] trainPoints;
    public GameObject[] exitPoints;
    public GameObject benchPointsParent;
    public GameObject ticketPointsParent;
    public GameObject vendingTicketsParent;
    public GameObject shopPointsParent;
    public GameObject[] speakers;
    public AudioClip[] trainAudio;
    public GameObject[] itemsAudio;
    [Range(0,100)]
    public int evacuationPerc = 50;
    public GameObject player;
    public Canvas[] canv;
    public GameObject[] mainDoors;
    public GameObject spawnersParent;
    public AudioSource alarm;
    public GameObject[] exitWalls;
    public GameObject alarmLight;
    public GameObject TrainFire;

    private float incrementLight = 0.015f;
    private Light alarmLightComponent;
    private float timeSinceAction;
    private bool firstAction;
    private bool startEvacuation;
    private GameObject[] vendingTicketsMachines;
    private GameObject[] trains;
    private GameObject[] benchPoints;
    private GameObject[] ticketPoints;
    private shop[] shops;
    private bool[] benchOccupied;
    private ArrayList clipsToPlay;
    private AudioSource[] speakersAS;
    private int charactersInScene;
    [SerializeField]
    private List<SerializableData> frames;
    private BinaryFormatter bf;
    private List<action> actionsDone;
    private float timeToSavePositions;
    private bool evacuating;
    private float totalSimulationTime;

    public class shop
    {
        public GameObject[] shopPoints;
        public bool[] pointOccupied;
    }

    public class action
    {
        public string act;
        public float time;
    }

    public static GameManager instance = null;

    void Awake () {
        dateAndTime = System.DateTime.Now;
        alarmLightComponent = alarmLight.GetComponent<Light>();

        //To calculate faster the paths for the agents
        NavMesh.pathfindingIterationsPerFrame = 500;
        //This is to avoid baking NavMesh onto the trains.
        GameObject[] l = GameObject.FindGameObjectsWithTag("Invisible");
        foreach (GameObject inv in l)
        {
            inv.SetActive(false);
            /*MeshRenderer mesh = inv.GetComponent<MeshRenderer>();
            if (mesh) mesh.enabled = false;*/
        }
        
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);

        /* GameObject data = GameObject.Find("simulationStart");

         if (data != null)
         {
             evacuationPerc = data.GetComponent<simulationStart>().percEvac;
             TIMETOEVACUATE = data.GetComponent<simulationStart>().evacuationTimeSeconds;
             execID = data.GetComponent<simulationStart>().executionID;
         }*/

        TIMETOEVACUATE = PlayerPrefs.GetInt("evacuationTime");
        evacuationPerc = PlayerPrefs.GetInt("percentage");
        execID = PlayerPrefs.GetInt("executionID");
       // Debug.Log(evacuationPerc + " " + TIMETOEVACUATE);

        firstAction = false;
        evacuating = false;
        startEvacuation = false;
        timeSinceAction = 0.0f;
        charactersInScene = 0;
        totalSimulationTime = 0.0f;
        //vendingTicketsMachines = GameObject.FindGameObjectsWithTag("vendingticket");
        trains = GameObject.FindGameObjectsWithTag("train");
        ticketPoints = new GameObject[ticketPointsParent.transform.childCount];
        vendingTicketsMachines = new GameObject[vendingTicketsParent.transform.childCount];
        benchPoints = new GameObject[benchPointsParent.transform.childCount];
        benchOccupied = new bool[benchPointsParent.transform.childCount];
        speakersAS = new AudioSource[speakers.Length];
        shops = new shop[shopPointsParent.transform.childCount];
        for (int i = 0; i < ticketPointsParent.transform.childCount; ++i) ticketPoints[i] = ticketPointsParent.transform.GetChild(i).gameObject;
        for (int i = 0; i < vendingTicketsParent.transform.childCount; ++i) vendingTicketsMachines[i] = vendingTicketsParent.transform.GetChild(i).gameObject;
        for (int i = 0; i < benchPointsParent.transform.childCount; ++i) benchPoints[i] = benchPointsParent.transform.GetChild(i).gameObject;
        for (int i = 0; i < benchOccupied.Length; ++i) benchOccupied[i] = false;
        for (int i = 0; i < speakers.Length; ++i) speakersAS[i] = speakers[i].GetComponent<AudioSource>();
        for (int i = 0; i < shopPointsParent.transform.childCount; ++i)
        {
            shops[i] = new shop();
            GameObject child = shopPointsParent.transform.GetChild(i).gameObject;
            shops[i].shopPoints = new GameObject[child.transform.childCount];
            shops[i].pointOccupied = new bool[child.transform.childCount];
            for (int j = 0; j < child.transform.childCount; ++j)
            {
                shops[i].shopPoints[j] = child.transform.GetChild(j).gameObject;
                shops[i].pointOccupied[j] = false;
            }
        }
        clipsToPlay = new ArrayList();

        frames = new List<SerializableData>();
        actionsDone = new List<action>();
        timeToSavePositions = 0.0f;
        initSerializableData();
        for (int i = 0; i < exitWalls.Length; ++i) exitWalls[i].SetActive(false);
        InvokeRepeating("storePositions",0.0f,FRAMERATE);
        //InvokeRepeating("visibilityPlayer",0.0f,Time.deltaTime);
    }

    private void PlayAlarmSound()
    {
       
        alarm.Play();
    }
	
	// Update is called once per frame
	void Update () {
        totalSimulationTime += Time.deltaTime;
        if (firstAction) timeSinceAction += Time.deltaTime;
         //Debug.Log("Temps que ha passat " + timeSinceAction);
 
        if (Input.GetKeyDown(KeyCode.G)) saveData();

        

        //debugTrain();
        if (!evacuating && (Input.GetKeyDown(KeyCode.B) || timeSinceAction > TIMETOEVACUATE || startEvacuation))
        {
            evacuating = true;
            //Evacuate people
            instance.TrainFire.SetActive(PlayerPrefs.GetInt("fireOn") == 1 ? true : false);
           
            GameObject[] peopleInStation = GameObject.FindGameObjectsWithTag("character");
            foreach (GameObject p in peopleInStation)
            {
                int num = Random.Range(0, 100);
                if (num <= evacuationPerc)
                {
                    p.GetComponent<CharacterControllerPHD>().setEvacuate();
                }
            }
            
            //Close main doors
            foreach (GameObject door in mainDoors)
            {
                door.GetComponent<Animator>().SetInteger("State",1);
            }

            //Evacuation alarm
            Invoke("PlayAlarmSound", 3);

            //Cancel NPC Generation
            for (int i = 0; i < spawnersParent.transform.childCount; ++i)
            {
                spawnersParent.transform.GetChild(i).GetComponent<CharacterSpawner>().cancelGeneration();
            }

            for (int i = 0; i < exitWalls.Length; ++i) exitWalls[i].SetActive(true);
        }
        if (evacuating)
            alarmLightFade();
        playClips();
	}

    private void alarmLightFade()
    {
        if (alarmLightComponent.intensity > 0.8f && incrementLight > 0)
            incrementLight = -incrementLight;
        if (alarmLightComponent.intensity < 0.5f && incrementLight < 0)
            incrementLight = -incrementLight;

        alarmLightComponent.intensity += incrementLight;
    }

    /*private void debugTrain()
    {
        for (int i = 0; i < 1; ++i)
        {
            Debug.Log("Train: " + i);
            //int peopleLeaving = trains[i].GetComponent<Train>().getPeopleLeaving();
            //int peopleEntering = trains[i].GetComponent<Train>().getPeopleEntering();
            int[] waitpointsOccupied = trains[i].GetComponent<Train>().getwaitpointsOccupied();
            int[] pointsOccupied = trains[i].GetComponent<Train>().getpointsinsideOccupied();
            //ArrayList peopleInside = trains[i].GetComponent<Train>().getpeopleinsideTrain();

            //Debug.Log("People leaving: " + peopleLeaving);
            //Debug.Log("People entering: " + peopleEntering);
            for (int j = 0; j < 1; ++j)
            {
                Debug.Log("Waitpoint " + j + " has " + waitpointsOccupied[j]);
            }
            for (int j = 0; j < pointsOccupied.Length; ++j)
            {
                Debug.Log("Point inside " + j + " has " + pointsOccupied[j]);
            }

            //Debug.Log("Train has " + peopleInside.Count + " people inside it now");
        }
    }*/

    private void playClips()
    {
        if (clipsToPlay.Count > 0)
        {
            if (!speakersAS[0].isPlaying)
            {
                for (int i = 0; i < speakersAS.Length; ++i)
                {
                    speakersAS[i].clip = clipsToPlay[0] as AudioClip;
                    speakersAS[i].Play();
                }
                clipsToPlay.RemoveAt(0);
            }
        }
    }

    public static int getMinimumOccupiedMachine()
    {
        int minpos = 0;
        for (int i=1;i<instance.vendingTicketsMachines.Length;i++)
        {
            if (instance.vendingTicketsMachines[i].GetComponent<VendingMachine>().getCharsInQueue() < instance.vendingTicketsMachines[minpos].GetComponent<VendingMachine>().getCharsInQueue())
                minpos = i;
        }
        return minpos;
    }

    public static Transform getTicketMachinePosition(out int mp)
    {
        mp = getMinimumOccupiedMachine();
        instance.vendingTicketsMachines[mp].GetComponent<VendingMachine>().AddCharacter();
        return instance.vendingTicketsMachines[mp].GetComponent<VendingMachine>().transform;
    }

    public static Vector3 getTicketPos(int ticketPoint)
    {
        return instance.ticketPoints[ticketPoint].transform.position;
    }

    public static void queueCharacter(GameObject c)
    {
        int m=c.GetComponent<CharacterControllerPHD>().getTicketmachine();
        instance.vendingTicketsMachines[m].GetComponent<VendingMachine>().AddCharacter(c);
    }

    public static void unqueueCharacter(int m)
    {
        instance.vendingTicketsMachines[m].GetComponent<VendingMachine>().UnqueueCharacter();
    }

    public static Transform getQueuePosition(int m)
    {
        return instance.vendingTicketsMachines[m].GetComponent<VendingMachine>().getNextPosition();
    }

    public static bool haySitio(int m)
    {
        return instance.vendingTicketsMachines[m].GetComponent<VendingMachine>().haySitio();
    }

    public static bool atLeastOneTicketMachineFree()
    {
        for (int i = 0; i < instance.vendingTicketsMachines.Length; ++i)
        {
            if (instance.vendingTicketsMachines[i].GetComponent<VendingMachine>().haySitio()) return true;
        }
        return false;
    }

    public static void getTicketMachinePosition(GameObject c)
    {
        GameObject t = GameObject.Find("VendingMachine");
        c.GetComponent<CharacterControllerPHD>().setDestination(t.GetComponent<VendingMachine>().getNextPosition().position);
    }

    public static bool someWaitzoneHasSpace()
    {
        for (int i = 0; i < instance.trains.Length; ++i)
        {
            if (instance.trains[i].GetComponent<Train>().someWaitPointFree()) return true;
        }
        return false;
    }

    public static int getTrainWithFewOccupiedWaitpoints()
    {
        int train = 0;
        int numAct = instance.trains[0].GetComponent<Train>().getTotalPeopleWaiting();
        for (int i = 1; i < instance.trains.Length; ++i)
        {
            int num = instance.trains[i].GetComponent<Train>().getTotalPeopleWaiting();
            if (num < numAct)
            {
                train = i;
                numAct = num;
            }
        }
        return train;
    }

    public static Vector3 getWaitpointPosition(out int waitpoint, out int train)
    {
        train = getTrainWithFewOccupiedWaitpoints();
        waitpoint = instance.trains[train].GetComponent<Train>().getWaitpoint();
        instance.trains[train].GetComponent<Train>().addCharacter(waitpoint);
        bool gotDest = false;

        Vector3 pos = new Vector3();
        while (!gotDest)
            gotDest = instance.trains[train].GetComponent<Train>().RandomPointCube(waitpoint,out pos);
        return pos;
    }

    public static bool isTrainWaiting(int train)
    {
        return instance.trains[train].GetComponent<Train>().isTrainWaiting();
    }

    public static void addEnteringTrain(int train)
    {
        instance.trains[train].GetComponent<Train>().addPeopleEntering();
    }

    public static bool noPeopleLeaving(int train)
    {
        return instance.trains[train].GetComponent<Train>().isPeopleLeaving();
    }

    public static bool hasEnoughSpace(int train)
    {
        return instance.trains[train].GetComponent<Train>().hasFreeSpace();
    }

    public static void subCharacter(int train, int waitpoint)
    {
        instance.trains[train].GetComponent<Train>().subCharacter(waitpoint);
    }

    public static Vector3 getTrainDestination(int train, Vector3 position)
    {
        Vector3 dest = instance.trains[train].GetComponent<Train>().getClosestFreeInsidePoint(position);
        return dest;
    }

    public static void addCharacterToTrain(int train, GameObject character)
    {
        instance.trains[train].GetComponent<Train>().addCharacterToList(character);
    }

    public static Vector3 getNearestWaitpoint(int train, Vector3 pos)
    {
        return instance.trains[train].GetComponent<Train>().getClosestWaitpoint(pos);
    }

    public static void subCharacterLeaving(int train)
    {
        instance.trains[train].GetComponent<Train>().subCharacterLeaving();
    }

    public static Vector3 findNearestTrainPoint(Vector3 pos)
    {
        GameObject tp = instance.trainPoints[0];
        float minDist = Vector3.Distance(tp.transform.position,pos);
        for (int i = 1; i < instance.trainPoints.Length; ++i)
        {

            float dist = Vector3.Distance(instance.trainPoints[i].transform.position, pos);
            if (dist < minDist)
            {
                tp = instance.trainPoints[i];
                minDist = dist;
            }
        }
        return tp.transform.position;
    }

    public static Vector3 getNearestExitPoint(Vector3 position)
    {
        GameObject exit = instance.exitPoints[0];
        float distMin = Vector3.Distance(position, exit.transform.position);
        for (int i = 1; i < instance.exitPoints.Length; ++i)
        {
            float dist = Vector3.Distance(instance.exitPoints[i].transform.position, position);
            if (dist < distMin)
            {
                exit = instance.exitPoints[i];
                distMin = dist;
            }
        }
        return exit.transform.position;
    }

    public static bool anyFreeBench(out Vector3 position, out int benchpoint)
    {
        benchpoint = -1;
        position = new Vector3();
        for (int i = 0; i < instance.benchOccupied.Length; ++i)
        {
            if (!instance.benchOccupied[i])
            {
                instance.benchOccupied[i] = true;
                benchpoint = i;
                position = instance.benchPoints[i].transform.position;
                return true;
            }
        }
        return false;
    }

    public static bool isAlignedWith(Transform transf, int point, string thing)
    {
        if (thing == "bench")
            return (Mathf.Abs(transf.eulerAngles.y - instance.benchPoints[point].transform.eulerAngles.y) < 10.0f);
        if (thing == "ticket")
        {
            return (Mathf.Abs(transf.eulerAngles.y - instance.ticketPoints[point].transform.eulerAngles.y) < 10.0f);
        }
        return false;
    }

    public static bool isAlignedWith(Transform transf, Transform point)
    {
        return (Mathf.Abs(transf.eulerAngles.y - point.eulerAngles.y) < 10.0f);
    }

    public static Transform getBenchPoint(int benchpoint)
    {
        return instance.benchPoints[benchpoint].transform;
    }

    public static void emptyBench(int benchpoint)
    {
        instance.benchOccupied[benchpoint] = false;
    }

    public static void callSpeakers(int numTren)
    {
        instance.clipsToPlay.Add(instance.trainAudio[numTren]);
    }

    public static Vector3 GetRandomExit()
    {
        int r = Random.Range(0,2);
        return instance.exitPoints[r].transform.position;
    }

    public static void addCharacterToScene()
    {
        ++instance.charactersInScene;
    }

    public static void addCharactersToScene(int num)
    {
        instance.charactersInScene += num;
    }

    public static void subCharacterInScene()
    {
        --instance.charactersInScene;
    }

    public static void subCharactersInScene(int num)
    {
        instance.charactersInScene -= num;
    }

    public static int getCharactersInScene()
    {
        return instance.charactersInScene;
    }

    public static bool getDirection(Transform transf, int ticketmachine)
    {
        Transform ticketPoint = instance.ticketPoints[ticketmachine].transform;
        if (transf.eulerAngles.y > ticketPoint.eulerAngles.y)
        {
            return ((transf.eulerAngles.y - ticketPoint.eulerAngles.y) < 180);
        }
        else 
        {
            return ((ticketPoint.eulerAngles.y - transf.eulerAngles.y) > 180);
        }
    }

    public static bool getDirection(Transform transf, Transform point)
    {
        if (transf.eulerAngles.y > point.eulerAngles.y)
        {
            return ((transf.eulerAngles.y - point.eulerAngles.y) < 180);
        }
        else 
        {
            return ((point.eulerAngles.y - transf.eulerAngles.y) > 180);
        }
    }

    public static void activateAudio(Transform transform, string type)
    {
        if (type == "toilet")
        {
            transform.GetComponent<AudioSource>().Play();
        }
        else if (type == "item")
        {

        }
    }

    public static bool anyShopFree(out Vector3 pos, out int shop, out int shopPoint)
    {
        for (int i = 0; i < 20; ++i)
        {
            int j = Random.Range(0,instance.shops.Length);
            int k = Random.Range(0,instance.shops[j].shopPoints.Length - 1);
            if (!instance.shops[j].pointOccupied[k])
            {
                instance.shops[j].pointOccupied[k] = true;
                pos = instance.shops[j].shopPoints[k].transform.position;
                shop = j;
                shopPoint = k;
                return true;
            }
        }
        pos = new Vector3(0,0,0);
        shop = -1;
        shopPoint = -1;
        return false;
    }

    public static Transform getShopPoint(int shop, int shopPoint)
    {
        return instance.shops[shop].shopPoints[shopPoint].transform;
    }

    public static Transform getBuyPoint(int shop)
    {
        int p = instance.shops[shop].shopPoints.Length;
        return instance.shops[shop].shopPoints[p - 1].transform;
    }

    public static bool isBuypointOccupied(out Vector3 pos, int shop)
    {
        int p = instance.shops[shop].shopPoints.Length;
        pos = instance.shops[shop].shopPoints[p - 1].transform.position;
        if (!instance.shops[shop].pointOccupied[p - 1])
        {
            instance.shops[shop].pointOccupied[p - 1] = true;
            return false;
        }
        return true;
    }

    public static void setShopPoint(int shop, int shopPoint, bool b)
    {
        instance.shops[shop].pointOccupied[shopPoint] = b;
    }

    public static void setShopPoint(int shop, bool b)
    {
        int p = instance.shops[shop].shopPoints.Length;
        instance.shops[shop].pointOccupied[p - 1] = b;
    }

    public static string SaveDataNow()
    {
        instance.saveData();
        return Application.persistentDataPath + "/npcData" + PlayerPrefs.GetInt("executionID") + ".dat";
    }
    private void saveData()
    {
        FileStream file = File.Open(Application.persistentDataPath + "/npcData" + execID + ".dat", FileMode.OpenOrCreate);

        bf.Serialize(file, frames);
        file.Close();
        
        StreamWriter sw = new StreamWriter(Application.persistentDataPath + "/variablesData" + execID + ".dat");
        sw.WriteLine(FRAMERATE);
        sw.WriteLine(totalSimulationTime);
        sw.WriteLine(execID);
        sw.WriteLine(dateAndTime);
        sw.Close();

        StreamWriter sw2 = new StreamWriter(Application.persistentDataPath + "/actionsData" + execID + ".dat");
        for (int i = 0; i < instance.actionsDone.Count; ++i)
        {
            string s = "Action " + (i + 1) + ": " + instance.actionsDone[i].act + ", Time: " + instance.actionsDone[i].time;
            sw2.WriteLine(s);
        }
        sw2.Close();
    }

    private void initSerializableData()
    {
        bf = new BinaryFormatter();
        SurrogateSelector surrogateSelector = new SurrogateSelector();
        Vector3SerializationSurrogate vector3SS = new Vector3SerializationSurrogate();

        surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3SS);
        bf.SurrogateSelector = surrogateSelector;
    }

    public static void AddAction(string thing)
    {
        action a = new action();
        a.act = thing;
        a.time = instance.totalSimulationTime;
        instance.actionsDone.Add(a);
    }

    public void storePositions()
    {
        SerializableData sd = new SerializableData();
        sd.pos = new List<Vector3>();
        sd.time = new List<float>();

        GameObject[] npcs = GameObject.FindGameObjectsWithTag("character");
        foreach (GameObject npc in npcs)
        {
            sd.pos.Add(npc.transform.position);
            sd.time.Add(timeToSavePositions);
        }
        sd.posPlayer = player.transform.position;
        sd.rotYPlayer = player.transform.localEulerAngles.y;
        sd.timePlayer = timeToSavePositions;
        frames.Add(sd);
        timeToSavePositions += FRAMERATE;
    }

    public void visibilityPlayer()
    {
        for (int i = 0; i < canv.Length; ++i)
        {
            Color tmp = canv[i].GetComponentInChildren<Image>().color;
            tmp.a -= 0.5f * Time.deltaTime;
            canv[i].GetComponentInChildren<Image>().color = tmp;
        }
        if (canv[0].GetComponentInChildren<Image>().color.a <= 0.0f)
        {
            CancelInvoke("visibilityPlayer");
        }
    }

    public static void StartCountdown()
    {
        instance.firstAction = true;
    }

    public static void StartEvacuating()
    {
        instance.startEvacuation = true;
    }

    public static void playItemSound(int n)
    {
        instance.itemsAudio[n].GetComponent<AudioSource>().Play();
    }

    private static IEnumerator countdown()
    {
        float fadeTime = instance.GetComponent<Fade>().startFade(1);
        yield return new WaitForSeconds(1 / fadeTime);
        instance.StopCoroutine(countdown());
        instance.saveData();
        
    }
}
