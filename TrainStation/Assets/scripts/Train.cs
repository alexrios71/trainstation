﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Train : MonoBehaviour {
    public bool departure;
    public GameObject waitpointsParent;
    public GameObject trainpointsParent;
    public int numTren;
    public new AudioClip[] audio;

    private GameObject[] waitpoints;
    private GameObject[] trainpoints;
    private ArrayList peopleInsideTrain;
    private AudioSource audioSource;

    public enum TrainState { is_out, coming, waiting, ready_to_go, leaving, closing, opening };
    
    private const int WAGONS = 3;
    private const float MINTIME = 60.0f;
    private const float MAXTIME = 180.0f;
    private const float WAITTIME = 2.0f;    //Must be greater than time for opening the doors
    private const int MAXCHARSWAITPOINT = 2;
    private const int MAXCHARSINSIDEPOINT = 3;
    private const int DESPAWNPERCERTAGE = 30;   //Between 0 and 100
    
    private Animator anim;
    private List<GameObject> doors;
    private List<GameObject> platforms;
    private TrainState state;
    private float timer;
    private float timerDoors;
    private int peopleEntering;         //So we can know when the train can leave. If there's anyone entering, the train waits.
    private int peopleLeaving;
    private bool speakersCalled;

    private int[] waitpointsOccupied;
    private int[] pointsInsideOccupied;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();

        //Initialize train
        state = TrainState.opening;
        timer = Random.Range(MINTIME, MAXTIME);
        timerDoors = WAITTIME;
        speakersCalled = false;
        doors = new List<GameObject>();
        platforms = new List<GameObject>();
        getAllDoorAndPlatformChilds(gameObject.transform);
        openDoors();   //This after setting all doors and platforms!
        peopleEntering = 0;
        peopleInsideTrain = new ArrayList();
        audioSource = GetComponent<AudioSource>();

        //get waitpoints and trainpoints
        waitpoints = new GameObject[waitpointsParent.transform.childCount];
        trainpoints = new GameObject[trainpointsParent.transform.childCount];
        for (int i = 0; i < waitpointsParent.transform.childCount; ++i) waitpoints[i] = waitpointsParent.transform.GetChild(i).gameObject;
        for (int i = 0; i < trainpointsParent.transform.childCount; ++i) trainpoints[i] = trainpointsParent.transform.GetChild(i).gameObject;

        waitpointsOccupied = new int[waitpoints.Length];
        pointsInsideOccupied = new int[trainpoints.Length];
        for (int i = 0; i < waitpointsOccupied.Length; ++i) waitpointsOccupied[i] = 0;
        for (int i = 0; i < pointsInsideOccupied.Length; ++i) pointsInsideOccupied[i] = 0;
    }
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        checkTrainsState();
    }

    private void checkTrainsState()
    {
        if (state == TrainState.coming && !anim.GetBool("IsComing"))
        {
            openDoors();
            state = TrainState.opening;
        }

        if (state == TrainState.waiting && timer <= 15.0f && !speakersCalled)
        {
            GameManager.callSpeakers(numTren);
            speakersCalled = true;
        }

        //If timer ends check train state if it is at the station or not
        if (timer <= 0.0f)
        {
            if (state == TrainState.is_out)
            {
                anim.SetInteger("State", 2);
                anim.SetBool("IsComing", true);

                //delete people in train (not all of them), then train is coming
                for (int i = 0; i < peopleInsideTrain.Count; ++i)
                {
                    int s = Random.Range(1,101);
                    if (s <= DESPAWNPERCERTAGE)
                    {
                        GameObject character = peopleInsideTrain[i] as GameObject;
                        peopleInsideTrain.RemoveAt(i);
                        Destroy(character);
                        --i;
                    }
                }
                GameManager.addCharactersToScene(peopleInsideTrain.Count);
                state = TrainState.coming;
                audioSource.clip = audio[0];
                audioSource.Play();
            }
            ///////////////////////////////////////
            /// by amyrneto@gmail.com
            /// This is meant to prevent the train from departuring, since it will burst into flames.
            /// 
       
            else if (state == TrainState.waiting && departure) state = TrainState.ready_to_go;
        }

        if (state == TrainState.ready_to_go)
        {
            //Check if no people left
            if (peopleEntering == 0)
            {
                //change parents of all people and deactivate Nav Mesh Agents
                for (int i = 0; i < peopleInsideTrain.Count; ++i)
                {
                    GameObject character = peopleInsideTrain[i] as GameObject;
                    character.transform.parent = transform;
                    character.GetComponent<NavMeshAgent>().enabled = false;
                }
                state = TrainState.closing;
                closeDoors();
            }
        }

        if (state == TrainState.leaving && !anim.GetBool("IsLeaving"))
        {
            //Remove number of chars inside the train from total chars in station
            GameManager.subCharactersInScene(peopleInsideTrain.Count);
            state = TrainState.is_out;
            timer = Random.Range(MINTIME, MAXTIME);
            speakersCalled = false;
        }

        if (state == TrainState.opening)
        {
            if (timerDoors <= 0.0f)
            {
                peopleLeaving = peopleInsideTrain.Count;
                for (int i = 0; i < pointsInsideOccupied.Length; ++i) pointsInsideOccupied[i] = 0;
                for (int i = 0; i < peopleInsideTrain.Count; ++i)
                {
                    GameObject character = peopleInsideTrain[i] as GameObject;
                    character.transform.parent = null;
                    character.GetComponent<NavMeshAgent>().enabled = true;
                }
                peopleInsideTrain.Clear();
                timerDoors = WAITTIME;
                state = TrainState.waiting;
                timer = Random.Range(MINTIME, MAXTIME);
            }
            else
            {
                timerDoors -= Time.deltaTime;
            }
        }

        if (state == TrainState.closing)
        {
            if (timerDoors <= 0.0f)
            {
                timerDoors = WAITTIME;
                anim.SetInteger("State", 1);
                anim.SetBool("IsLeaving", true);
                state = TrainState.leaving;
                audioSource.clip = audio[1];
                audioSource.Play();
            }
            else
            {
                timerDoors -= Time.deltaTime;
            }
        }
    }

    private void getAllDoorAndPlatformChilds(Transform parent)
    {
        foreach (Transform child in parent)
        {
            if (child.name == "AnimatorDoorRenfe")
            {
                doors.Add(child.gameObject);
            }
            if (child.name == "AnimatorPlatformRenfe")
            {
                platforms.Add(child.gameObject);
            }
            getAllDoorAndPlatformChilds(child);
        }
    }

    private void closeDoors()
    {
        //Close doors
        foreach (GameObject door in doors)
        {
            door.GetComponent<Animator>().SetInteger("State", 2);
        }

        //Put in platforms
        foreach (GameObject platform in platforms)
        {
            platform.GetComponent<Animator>().SetInteger("State", 1);
        }
    }

    private void openDoors()
    {
        //Open doors
        foreach (GameObject door in doors)
        {
            door.GetComponent<Animator>().SetInteger("State", 1);
        }

        //Put out platforms
        foreach (GameObject platform in platforms)
        {
            platform.GetComponent<Animator>().SetInteger("State", 2);
        }
    }

    public void setIsLeavingTrue()
    {
        anim.SetBool("IsLeaving", true);
    }

    public void setIsLeavingFalse()
    {
        anim.SetBool("IsLeaving", false);
    }

    public void setIsComingTrue()
    {
        anim.SetBool("IsComing", true);
    }

    public void setIsComingFalse()
    {
        anim.SetBool("IsComing", false);
    }

    public bool someWaitPointFree()
    {
        for (int i = 0; i < waitpointsOccupied.Length; ++i)
        {
            if (waitpointsOccupied[i] < MAXCHARSWAITPOINT) return true;
        }
        return false;
    }

    public int getTotalPeopleWaiting()
    {
        int sum = 0;
        for (int i = 0; i < waitpointsOccupied.Length; ++i) sum += waitpointsOccupied[i];
        return sum;
    }

    public void addCharacter(int waitpoint)
    {
        ++waitpointsOccupied[waitpoint];
    }

    public int getWaitpoint()
    {
        int waitpoint = 0;
        for (int i = 1; i < waitpoints.Length; ++i)
        {
            if (waitpointsOccupied[i] < waitpointsOccupied[waitpoint]) waitpoint = i;
        }
        return waitpoint;
    }

    public Transform getPositionWaitpoint(int waitpoint)
    {
        return waitpoints[waitpoint].transform;
    }

    public bool RandomPointCube(int waitpoint, out Vector3 pos)
    {
        Transform c = waitpoints[waitpoint].transform.Find("Cube");
        Vector3 center = c.position;
        Vector3 scale = c.localScale;

        for (int i = 0; i < 50; i++)
        {
            Vector3 rand = new Vector3(Random.Range(-scale.x / 2, scale.x / 2), Random.Range(-scale.y / 2, scale.y / 2), Random.Range(-scale.z / 2, scale.z / 2)) + center;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(rand, out hit, 1.0f, NavMesh.AllAreas))
            {
                pos = hit.position;
                return true;
            }
        }
        pos = Vector3.zero;
        return false;
    }

    public bool isTrainWaiting()
    {
        return (state == TrainState.waiting);
    }

    public void addPeopleEntering()
    {
        ++peopleEntering;
    }

    public bool isPeopleLeaving()
    {
        return (peopleLeaving == 0);
    }

    public bool hasFreeSpace()
    {
        for (int i = 0; i < pointsInsideOccupied.Length; ++i)
        {
            if (pointsInsideOccupied[i] < MAXCHARSINSIDEPOINT) return true;
        }
        return false;
    }

    public void subCharacter(int waitpoint)
    {
        --waitpointsOccupied[waitpoint];
    }

    public Vector3 getClosestFreeInsidePoint(Vector3 position)
    {
        GameObject closestPoint = trainpoints[0];
        float minDist = Vector3.Distance(position, closestPoint.transform.position);
        int k = 0;

        for (int i = 1; i < pointsInsideOccupied.Length; ++i)
        {
            float dist = Vector3.Distance(position, trainpoints[i].transform.position);
            if (pointsInsideOccupied[i] < MAXCHARSINSIDEPOINT && dist < minDist)
            {
                k = i;
                closestPoint = trainpoints[i];
                minDist = dist;
            }
        }

        ++pointsInsideOccupied[k];

        return closestPoint.transform.position;
    }

    public void addCharacterToList(GameObject character)
    {
        peopleInsideTrain.Add(character);
        --peopleEntering;
    }

    public Vector3 getClosestWaitpoint(Vector3 pos)
    {
        GameObject waitpoint = waitpoints[0];
        float minDist = Vector3.Distance(pos,waitpoint.transform.position);
        for (int i = 1; i < waitpoints.Length; ++i)
        {
            float dist = Vector3.Distance(pos, waitpoints[i].transform.position);
            if (dist < minDist)
            {
                waitpoint = waitpoints[i];
                minDist = dist;
            }
        }
        return waitpoint.transform.position;
    }

    public void subCharacterLeaving()
    {
        --peopleLeaving;
    }

    public int getPeopleLeaving()
    {
        return peopleLeaving;
    }

    public int getPeopleEntering()
    {
        return peopleEntering;
    }

    public int[] getwaitpointsOccupied()
    {
        return waitpointsOccupied;
    }

    public int[] getpointsinsideOccupied()
    {
        return pointsInsideOccupied;
    }

    public ArrayList getpeopleinsideTrain()
    {
        return peopleInsideTrain;
    }
}
