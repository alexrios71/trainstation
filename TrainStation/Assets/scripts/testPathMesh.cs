﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class testPathMesh : MonoBehaviour {

    NavMeshAgent agent;
    NavMeshPath path;
    public Vector3 firstCorner;
    public Vector3 nextCorner;

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        if (agent.hasPath)
        {
            path = agent.path;
            firstCorner = path.corners[0];
            nextCorner = path.corners[1];
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            path = agent.path;
            path.corners[1] = new Vector3(5.0f,0.1f,85.0f);
            agent.path = path;
        }
	}
}
