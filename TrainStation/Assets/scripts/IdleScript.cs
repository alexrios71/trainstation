﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IdleScript : MonoBehaviour
{
    private Animator anim;
    private NavMeshAgent agent;

    private float time = 0.0f;
    private float interp = 0.0f;
    private int lastIdleState = 1;
    private int idleState = 1;
    private bool switchIdle = false;
    private float min = 0.0f;
    private float max = 1.0f;

    private float MaxTime = 10.0f;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        anim.SetFloat("Idle1", 1.0f);
    }

    void Update()
    {
        if (agent.isActiveAndEnabled && agent.remainingDistance <= agent.stoppingDistance && anim.GetInteger("State") == 0)
        {
            time += Time.deltaTime;

            if (time > MaxTime)
            {
                time = 0.0f;
                if (!switchIdle)
                {

                    idleState = Random.Range(1, 7);
                    if (idleState != lastIdleState) switchIdle = true;
                }
            }

            if (switchIdle)
            {
                anim.SetFloat("Idle" + lastIdleState, Mathf.Lerp(max, min, interp));
                anim.SetFloat("Idle" + idleState, Mathf.Lerp(min, max, interp));
                interp += Time.deltaTime;

                if (interp > 1.2f)
                {
                    lastIdleState = idleState;
                    switchIdle = false;
                    interp = 0.0f;
                }
            }
        }
        else
        {
            time = 0.0f;
            if (anim.GetFloat("Idle1") < 1.0f)
            {
                if (lastIdleState == 1)
                {
                    anim.SetFloat("Idle1", 1);
                    anim.SetFloat("Idle" + idleState, 0);
                }
                else if (idleState == 1)
                {
                    anim.SetFloat("Idle" + lastIdleState, 0);
                    anim.SetFloat("Idle1", 1);
                }
                else
                {
                    anim.SetFloat("Idle1", 1);
                    anim.SetFloat("Idle" + lastIdleState, 0);
                    anim.SetFloat("Idle" + idleState, 0);
                }
            }
            switchIdle = false;
            interp = 0.0f;
            lastIdleState = 1;
            idleState = 1;
        }

    }
}
