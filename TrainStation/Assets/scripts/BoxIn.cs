﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BoxIn : MonoBehaviour {

    private List<GameObject> charsToEnter;

    // Use this for initialization
    void Start () {
        charsToEnter = new List<GameObject>();
    }
	
	// Update is called once per frame
	void Update () {

        //If there is people that went inside but it's still in the entering zone, check everyone until
        //they are inside and change inside state.
        if (charsToEnter.Count > 0)
        {
            for (int i = 0; i < charsToEnter.Count; ++i)
            {
                NavMeshAgent agent = charsToEnter[i].GetComponent<NavMeshAgent>();
                NavMeshHit hit;
                if (!NavMesh.SamplePosition(agent.transform.position, out hit, 0.1f, 16))
                {
                    charsToEnter[i].GetComponent<ControllerMovementAI>().setInside(true);
                    agent.areaMask = (37 | (agent.areaMask & 8));
                    charsToEnter.RemoveAt(i);
                    --i;
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "character" && !other.gameObject.GetComponent<ControllerMovementAI>().getInside())
        {
            NavMeshAgent agent = other.gameObject.GetComponent<NavMeshAgent>();
            NavMeshHit hit;
            if (!NavMesh.SamplePosition(agent.transform.position, out hit, 0.1f, 16))
            {
                other.gameObject.GetComponent<ControllerMovementAI>().setInside(true);
                agent.areaMask = (37 | (agent.areaMask & 8));
            }
            else
            {
                charsToEnter.Add(other.gameObject);
            }
        }
    }
}
