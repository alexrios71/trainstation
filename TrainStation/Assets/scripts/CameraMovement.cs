﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    private Vector3 cameraPos;

    public GameObject cam;
    public Transform target;

	// Use this for initialization
	void Start () {
		
	}


    // LateUpdate so it is updated after the SteamVR scripts
    void LateUpdate () {

        cameraPos = cam.transform.localPosition;
        transform.position = target.position - cameraPos;
        /*Vector3 originPos = SteamVR_Render.Top().origin.position;
        SteamVR_Render.Top().origin.position = new Vector3(target.position.x, originPos.y, target.position.z);*/
    	}

    public void updateTarget(Transform t)
    {

    }
}
