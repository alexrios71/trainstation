﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class endSimulation : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            SceneManager.LoadScene("EndScene");
        }
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void SaveData()
    {
        string fn=GameManager.SaveDataNow();
        GameObject.Find("FileNameSave").GetComponent<Text>().text=fn;

    }
}
