﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectorProxy : MonoBehaviour {

    public GameObject proxy;

    private int counter;
    private Proxy p;
    private List<GameObject> chars;

	// Use this for initialization
	void Start () {
        counter = 0;
        p = proxy.GetComponent<Proxy>();
        chars = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
        if (counter > 0)
        {
            //Activar gameobject
            proxy.SetActive(true);
            if (!p.isActive()) p.setAct(true);
        }
        else if (counter == 0 && proxy.activeSelf && p.isActive())
        {
            p.setAct(false);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "character")
        {
            if (other.gameObject.GetComponent<MovementAI>().getInside())
            {
                chars.Add(other.gameObject);
                ++counter;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "character")
        {
            if (chars.Contains(other.gameObject))
            {
                chars.Remove(other.gameObject);
                --counter;
            }
        }
    }

    public bool isInList(GameObject c)
    {
        return chars.Contains(c);
    }
}
