using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for
        public bool onlyRotate;

        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();
	        agent.updateRotation = true;
	        agent.updatePosition = true;
            onlyRotate = false;
        }


        private void Update()
        {
            if (target != null)
                agent.SetDestination(target.position);

            if (agent.isActiveAndEnabled && agent.remainingDistance > agent.stoppingDistance)
            {
                character.Move(agent.desiredVelocity, false, false, onlyRotate);
            }
                
            else if (agent.isActiveAndEnabled)
                character.Move(Vector3.zero, false, false, onlyRotate);
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
}
