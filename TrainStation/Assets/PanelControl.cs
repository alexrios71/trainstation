﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RaycastHit hit;
        int layerMask = 1 << LayerMask.NameToLayer("objects");
        Ray ray=new Ray(transform.parent.position, transform.parent.forward);
        if (Physics.Raycast(ray, out hit, 5.0f, layerMask))
        {
            Vector3 t = transform.parent.position + transform.parent.forward * Mathf.Clamp(0.8f*hit.distance, 0.5f, 1.5f);
            transform.position = t;
            //Debug.DrawLine(ray.origin, hit.point);
        }
        else
        {
            Vector3 t = transform.parent.position + transform.parent.forward * 1.5f;
            transform.position = t;
        }
    }
}
