﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {
    public GameObject avatar;
    public float displacement, avatarHeight;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 t = new Vector3(avatar.transform.position.x, (avatar.transform.position.y + avatarHeight) * avatar.transform.localScale.y, avatar.transform.position.z);
        t = t + avatar.transform.forward * displacement;
        transform.position = t;
	}
}
